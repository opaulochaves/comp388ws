

COMP388 Web Services - E-Commerce
=========================================

This application is using followings:

- Spring framework (Spring boot)
- Jersey framework (JAX-RS)
- Hibernate
- Tomcat

E-Commerce Web Application is a simple application for managing (creating, updating, deleting, 

reserving) sellers, products and customers.

First-time Setup
-------
1. Go to your workspace directory
> `$ cd path/to/your/workspace`
2. Clone the project from Bitbucket
> `$ git clone https://{yourUserName}@bitbucket.org/paulochavesbr/comp388ws.git`

3. Open Eclipse and import the project as an existing Maven Project
> `File > Import > Maven > Existing Maven Projects`
> Browse for the project you just cloned which contains a `pom.xml` file and click on `Finish`. 

After that the Maven plugin that comes with Eclipse will take care of downloading all dependencies 

for you

4. Create a copy of `src/main/resources/example-application.properties` and paste it in the same 

folder. Name the copy as `src/main/resources/application.properties`
5. Open `src/main/resources/application.properties` and set your MySql username and password
6. Open the MySql server and create a database called `comp388ws`
7. Now you can run the project. Right click on `com.comp388ws.ecommerce.EcommerceApplication.java` 

and click on `Run As > Java Application`
8. After you run it for the first time, if you want to have some data to get started you may import 

`src/main/resources/import.sql` on your Mysql server. Go to the `resources` directory, log in on 

MySql and import `import.sql` file:

> `$ cd /{YourWorkspace}/comp388ws/src/main/resources/`
>
> `$ mysql -u {username} -p`
>
> `$ \. import.sql`



Contents
--------

The application consists of 5 main resource classes:

> `com.comp388ws.ecommerce.service.CustomerResource.java`

> `com.comp388ws.ecommerce.service.SellerResource.java`

> `com.comp388ws.ecommerce.service.ProductResource.java`

> `com.comp388ws.ecommerce.service.OrderResource.java`

> `com.comp388ws.ecommerce.service.CartResource.java`

All the application JAX-RS resources are deployed under the same `api`root path. The mapping of the 

URI path space for the individual
resources is presented in the following table:


URI path                           | Resource class      | HTTP methods | Description
---------------------------------- | ------------------- | ------------ | ----------------------
**/customers**                     | CustomerResource     | GET          | Retrieve list of customers
**/customers**                     | CustomerResource     | POST         | Create a new customer (input: `application/json`; output: text/html);
**/customers/{customerId}**                | CustomerResource     | GET          | Retrieve detailed information about specified customer.
**/customers/{customerId}** | CustomerResource | PUT | Updates the specified customer.
**/customers/{customerId}**                | CustomerResource     | DELETE       | Delete a single customer
**/customer/{customerId}/login** | CustomerResource | GET | Returns the login of the customer.
**/customers/{customerId}/addresses**    | CustomersResource     | POST         | Adds the address to the customer.
**/customers/{customerId}/addresses** | CustomerResource | GET | Returns the addresses of the customer.
**/customers/{customerId}/addresses/{id}** | CustomerResource | GET | Returns the specified address of the customer.
**/customers/{customerId}/addresses/{id}** | CustomerResource | PUT | Updates the specified address of the customer.
**/customers/{customerId}/addresses/{id}** | CustomerResource | DELETE | Removes the address of the customer.
**/customer/{customerId}/creditcard**         | CustomerResource     | POST         | Adds credit card information to the customer
**/customers/{customerId}/creditcard** | CustomerResource | GET | Returns the credit card of the customer.
**/customers/{customerId}/creditcard** | CustomerResource | PUT | Updates credit card of the customer.
**/customers/{customerId}/creditcard** | CustomerResource | DELETE | removes the credit card of the customer.
**/sellers** | SellerResource   | GET          | Retrieve list of sellers
**/sellers** | SellerResource   | POST         | Create a new selller(input: `application/json`; output: text/html)
**/sellers/{sellerId}** | SellerResource | GET | Returns the specified seller information.
**/sellers/{sellerId}/login** | SellerResource   | GET          | Retrieve login information of the seller.
**/sellers/{sellerId}/products** | SellerResource | GET | Lists all the  products of the seller.
**/sellers/{sellerId}** | SellerResource | PUT | Updates the seller information.
**/sellers/{sellerId}** | SellerResource   | DELETE       | Delete a seller
**/sellers/{sellerId}/reviews** | SellerResource | GET | List all the reviews of the seller.
**/sellers/{sellerId}/reviews** | SellerResource | POST | create a review for the seller.
**/sellers/{sellerId}/reviews/{id}** | SellerResource | GET | Retrieve the specified review of the seller.
**/sellers/{sellerId}/orders/fulfillment**   | OrderResource | GET | Get the list of orders to be fulfilled
**/sellers/{sellerId}/orders/{orderId}/fulfillment/{itemId}/{trackingNumber}** | OrderResource | POST | Fullfill an item of the Order
**/products**                  | ProductResource   | POST         | Creates a new product
**/products/{productId}**           | ProductResource   | GET          | Receive product information.
**/products** | ProductResource | GET | Lists all the products.
**/products/{productId}** | ProductResource | PUT | Updates the product.
**/products/{productId}/details** | ProductResource | POST | Creates a detail to the product.
**/products/{productId}/details** | ProductResource | GET | Lists all the details related to the product.
**/products/{productId}/details/{detailId}** | ProductResource | PUT | Updates the specified detail of the product.
**/products/{productId}/details/{detailId}** | ProductResource | DELETE | Removes the speified detail of the product.
**/products/{productId}/details** | ProductResource | DELETE | Removes all the product details of the specified product.
**/products/{productId}/reviews** | ProductResource | GET | List all the reviews of the specified product.
**/products/{productId}/reviews** | ProductResource | POST | Creates a review for the specified product.
**/customers/{id}/cart** | CartResource | GET | Returns the content of the cart of the customer.
**/customers/{id}/cart** | CartResource | POST | Adds an item to the cart of the customer.
**/customers/{id}/cart/{cartId}** | CartResource | PUT | updates an item in the cart.
**/customers/{id}/cart/{cartId}** | CartResource | DELETE | removes an item from the cart.
**/customers/{id}/orders** | CustomerResource | GET | List all the orders of the customer.
**/categories** | CategoryResource | GET | Lists all the categories.
/**categories** | CategoryResource | POST | Creates a new category.
**/categories/{id}** | CategoryResource | GET | Returns the specified category.
**/categories/{id}** | CategoryResource | PUT | Updates the specified category.
**/orders** | OrderResource | POST | Creates an order for the customer from the items in the cart.
**/orders/{orderId}** | OrderResource | GET | Returns the specified order of the customer.
**/orders/{orderId}** | OrderResource | DELETE | Cancels the order if it is not shipped yet.
**/orders/{orderId}/items** | OrderResource | POST | Adds an item to the orders
**/orders/{orderId}/items/{productId}** | OrderResource | DELETE | Removes a product from the order.
**/orders/{orderId}/payment** | OrderResource | POST | Pays for the specified order of the customer.
**/orders/{orderId}/status** | OrderResource | GET | Returns the status of the specified order.

JSON formats of the objects when posting a new one
---------------------------

CUSTOMER
```
{
    "first_name":"Customer",
	"last_name":"First",
	"username":"customer",
	"email":"customer@email.com",
	"phone":"9999999999",
	"password":"12345"
}
```
CREDIT CARD
```
{
    "card_type": "Visa",
    "card_number": "9999999999999999",
    "security_code": "999",
    "name_on_card": "CUSTOMER NAME",
    "exp_month": "12",
    "exp_year": "2020"
}
```
ADDRESS
```
{
    "type": "Home",
    "line1": "9999 N. Somewhere St",
    "line2": "",
    "city": "Chicago",
    "state": "IL",
    "zipcode": "99999",
    "country": "US"
}
```

SELLER
```
{
    "email": "seller@email.com",
    "phone": "9999999999",
    "username": "",
    "password": "password",
    "first_name": "first name",
    "last_name": "last name",
    "company_name": "Company name",
    "type": "Personal"
}
```
CATEGORY
```
{     
    "name": "Name",
    "description": "description"
}
```

PRODUCT
```
{
    "name": "Product Name",
    "description": "Product description",
    "unit_price": 100,
    "quantity": 10,
    "brand_name": "Brand Name",
    "category": 1,
    "seller": 1
}
```
CART ITEM
```
{
    "product": 1,
    "quantity": 1
}
```


References
-----
- [Spring Boot Reference Guide](http://docs.spring.io/spring-

boot/docs/current/reference/htmlsingle/)

- [Spring Data JPA Reference](https://docs.spring.io/spring-data/jpa/docs/current/reference/html/)

- [Github repository - Spring Data Samples](https://github.com/spring-projects/spring-data-

examples/)

- [Github repository - Demo REST Jersey Spring](https://github.com/Codingpedia/demo-rest-jersey-

spring)

- [Error handling in REST API with Jersey](http://www.codingpedia.org/ama/error-handling-in-rest-

api-with-jersey/)

- [Stackoverflow - Jersey ExceptionMapper doesn't map exceptions]

(http://stackoverflow.com/questions/27982948/jersey-exceptionmapper-doesnt-map-exceptions)

- [Spring Data Repository does not delete ManyToOne Entity]

(http://stackoverflow.com/questions/29172313/spring-data-repository-does-not-delete-manytoone-

entity)

- [Getting Started with Jersey and Spring Boot](http://blog.codeleak.pl/2015/01/getting-started-

with-jersey-and-spring.html)
