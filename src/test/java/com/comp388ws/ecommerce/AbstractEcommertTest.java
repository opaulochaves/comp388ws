package com.comp388ws.ecommerce;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.transaction.BeforeTransaction;
import org.springframework.transaction.annotation.Transactional;

import com.comp388ws.ecommerce.AbstractEcommertTest.TestConfig;

@SpringApplicationConfiguration(classes = TestConfig.class)
@Transactional
public abstract class AbstractEcommertTest extends AbstractTransactionalJUnit4SpringContextTests {

	@Configuration
	@EnableAutoConfiguration
	@ComponentScan
	static class TestConfig {

	}
	
	@BeforeTransaction
	public void setupData() throws Exception {
		deleteFromTables("customers", "sellers", "users");
		executeSqlScript("classpath:data/data.sql", false);
	}

}
