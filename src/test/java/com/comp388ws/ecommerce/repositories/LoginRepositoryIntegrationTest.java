package com.comp388ws.ecommerce.repositories;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.comp388ws.ecommerce.AbstractEcommertTest;
import com.comp388ws.ecommerce.model.Login;

/**
 * Integration tests for Spring Data JPA {@link LoginRepository}.
 * 
 * @author Paulo Chaves
 *
 */
public class LoginRepositoryIntegrationTest extends AbstractEcommertTest {

	@Autowired
	LoginRepository repository;

	/**
	 * Test if the repository return all stored users
	 * 
	 * @throws Exception
	 */
	@Test
	public void findAllUsers() throws Exception {
		Iterable<Login> result = repository.findAll();

		assertThat(result, is(notNullValue()));
		assertTrue(result.iterator().hasNext());
	}

	/**
	 * Test if there is a user with a fullname John
	 * 
	 * @throws Exception
	 */
	@Test
	public void findUserWithFullnameEqualsToJohn() throws Exception {
		//List<Login> list = repository.findByFullnameContaining("John");
		//assertThat(list.size(), is(1));
		//assertTrue(list.get(0).getFullname().equals("John"));
	}
}
