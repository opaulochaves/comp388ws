package com.comp388ws.ecommerce;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.web.WebAppConfiguration;

import com.comp388ws.ecommerce.EcommerceApplication;

import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = EcommerceApplication.class)
@WebAppConfiguration
public class EcommerceApplicationTests {

	@Test
	public void contextLoads() {
	}

}
