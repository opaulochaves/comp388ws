insert into users(id, fullname, email, password, active, admin) values(1, 'Super', 'super@user.com', '12345', b'1', b'1');
insert into users(id, fullname, email, password, active, admin) values(2, 'Matthew', 'matthew@user.com', '12345', b'1', b'0');
insert into users(id, fullname, email, password, active, admin) values(3, 'John', 'john@user.com', '12345', b'1', b'0');
insert into users(id, fullname, email, password, active, admin) values(4, 'Luke', 'luke@usercom', '12345', b'1', b'0');
insert into users(id, fullname, email, password, active, admin) values(5, 'David', 'david@user.com', '12345', b'1', b'0');

insert into customers(id, users_id, phone) values(1, 2, '1112221111');
insert into customers(id, users_id, phone) values(2, 4, '3332221111');

insert into sellers(id, users_id, phone, sellers_type) values(1, 3, '1113332222', 'Personal');
insert into sellers(id, users_id, phone, sellers_type) values(2, 5, '2223332222', 'Personal');