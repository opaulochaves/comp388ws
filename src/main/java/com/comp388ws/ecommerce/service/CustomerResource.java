package com.comp388ws.ecommerce.service;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.comp388ws.ecommerce.errors.AppException;
import com.comp388ws.ecommerce.model.Login;
import com.comp388ws.ecommerce.representation.AddressR;
import com.comp388ws.ecommerce.representation.CreditCardR;
import com.comp388ws.ecommerce.representation.CustomerR;
import com.comp388ws.ecommerce.representation.OrderR;
import com.comp388ws.ecommerce.workflow.CustomerActivity;
import com.comp388ws.ecommerce.workflow.LoginActivity;
import com.comp388ws.ecommerce.workflow.OrderActivity;

@Component
@Path("/customers")
public class CustomerResource {

	@Autowired
	private CustomerActivity customerActivity;

	@Autowired
	private LoginActivity loginActivity;

	@Autowired
	private OrderActivity orderActivity;

	@Context
	UriInfo uri;

	/**
	 * Creates a new customer
	 * 
	 * @param customerLogin
	 * @return
	 */
	@POST
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public Response createCustomer(CustomerR customerR) {
		customerR = customerActivity.create(customerR);
		addLinkToCustomerR(customerR);
		return Response.status(Response.Status.CREATED).entity(customerR)
				.header("Location", uri.getBaseUri().toString() + "customers/" + customerR.getId()).build();

	}

	/**
	 * Returns a single customer given his id
	 * 
	 * @param id
	 * @return
	 * @throws AppException
	 */
	@GET
	@Path("/{id}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response getById(@PathParam("id") Long id) throws AppException {
		CustomerR customerR = customerActivity.getById(id);
		addLinkToCustomerR(customerR);
		return Response.status(200).entity(customerR).build();
	}

	/**
	 * Get all customers
	 * 
	 * @return
	 */
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public List<CustomerR> getCustomers() {
		List<CustomerR> customersR = customerActivity.getAll();
		for (CustomerR customer : customersR) {
			addLinkToCustomerR(customer);
		}
		return customersR;
	}

	private void addLinkToCustomerR(CustomerR customer) {
		// link to itself
		String hrefSelf = uri.getBaseUriBuilder().path(CustomerResource.class).path(Long.toString(customer.getId()))
				.build().toString();
		customer.addLink(hrefSelf, "self", "get");

		// link to cart
		String hrefCart = uri.getBaseUriBuilder().path(CartResource.class).path(CartResource.class, "getCart")
				.resolveTemplate("id", customer.getId()).build().toString();
		customer.addLink(hrefCart, "cart", "get");

		// link to orders
		String hrefOrders = uri.getBaseUriBuilder().path(CustomerResource.class)
				.path(CustomerResource.class, "getOrders").resolveTemplate("customerId", customer.getId()).build()
				.toString();
		customer.addLink(hrefOrders, "order", "get");

		// link to addresses
		String hrefAddress = uri.getBaseUriBuilder().path(CustomerResource.class)
				.path(CustomerResource.class, "getAddresses").resolveTemplate("customerId", customer.getId()).build()
				.toString();
		customer.addLink(hrefAddress, "address", "get");

		// link to credit card
		String hrefCreditCard = uri.getBaseUriBuilder().path(CustomerResource.class)
				.path(CustomerResource.class, "getCreditCard").resolveTemplate("id", customer.getId()).build()
				.toString();
		customer.addLink(hrefCreditCard, "creditcard", "get");
	}

	@PUT
	@Path("/{id}")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public Response updateCustomer(@PathParam("id") Long id, CustomerR customerR) throws AppException {
		customerR.setId(id);
		customerR = customerActivity.update(customerR);
		addLinkToCustomerR(customerR);
		return Response.status(200).entity(customerR).build();
	}

	/**
	 * Gets the login info of a given customer
	 * 
	 * @param id
	 * @return
	 * @throws AppException
	 */
	@GET
	@Path("/{id}/login")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response getLogin(@PathParam("id") Long customerId) throws AppException {
		CustomerR customerR = customerActivity.getById(customerId);
		Login login = loginActivity.getByCustomer(customerR);
		return Response.status(200).entity(login).build();
	}

	/**
	 * Add a credit card to a customer
	 * 
	 * @param creditCard
	 * @return
	 * @throws AppException
	 */
	@POST
	@Path("/{id}/creditcard")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.TEXT_HTML })
	public Response addCreditCard(@PathParam("id") Long customerId, CreditCardR creditCardR) throws AppException {
		CustomerR customerR = customerActivity.getById(customerId);
		creditCardR = customerActivity.addCreditCard(customerR, creditCardR);

		return Response.status(201).entity("A credit card has been added to the customer.")
				.header("Location", uri.getAbsolutePath()).build();
	}

	/**
	 * Get the credit card info of a given customer
	 * 
	 * @param customerId
	 * @return
	 * @throws AppException
	 */
	@GET
	@Path("/{id}/creditcard")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response getCreditCard(@PathParam("id") Long customerId) throws AppException {
		CustomerR customerR = customerActivity.getById(customerId);
		CreditCardR creditCardR = customerActivity.getCreditCard(customerR);
		return Response.status(200).entity(creditCardR).build();
	}

	@PUT
	@Path("/{id}/creditcard")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response updateCreditCard(@PathParam("id") Long customerId, CreditCardR creditCardR) throws AppException {
		CustomerR customerR = customerActivity.getById(customerId);
		creditCardR = customerActivity.updateCreditCard(customerR, creditCardR);
		return Response.status(200).entity(creditCardR).build();
	}

	@DELETE
	@Path("/{id}/creditcard")
	@Produces({ MediaType.TEXT_HTML })
	public Response deleteCreditCard(@PathParam("id") Long customerId) throws AppException {
		CustomerR customerR = customerActivity.getById(customerId);
		customerActivity.deleteCreditCard(customerR);
		return Response.status(204).entity("The credit card has been removed from the customer's account.").build();
	}

	/**
	 * Returns all addresses of a customer
	 * 
	 * @param customerId
	 * @return
	 * @throws AppException
	 */
	@GET
	@Path("/{customerId}/addresses")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<AddressR> getAddresses(@PathParam("customerId") Long customerId) throws AppException {
		List<AddressR> addresses = customerActivity.getAddresses(customerId);
		return addresses;
	}

	/**
	 * Returns the address of a customer
	 * 
	 * @param customerId
	 * @param id
	 * @return
	 * @throws AppException
	 */
	@GET
	@Path("/{customerId}/addresses/{id}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response getAddress(@PathParam("customerId") Long customerId, @PathParam("id") int id) throws AppException {
		AddressR addressR = customerActivity.getAddress(customerId, id);
		return Response.status(200).entity(addressR).build();
	}

	@POST
	@Path("/{customerId}/addresses")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.TEXT_HTML })
	public Response addAddress(@PathParam("customerId") Long customerId, AddressR addressR) throws AppException {
		addressR = customerActivity.addAddress(customerId, addressR);
		return Response.status(201).entity("An address has been added to the customer.")
				.header("Location", uri.getAbsolutePath()).build();
	}

	/**
	 * 
	 * @param customerId
	 * @param id
	 * @param addressR
	 * @return
	 * @throws AppException
	 */
	@PUT
	@Path("/{customerId}/addresses/{id}")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public Response updateAddress(@PathParam("customerId") Long customerId, @PathParam("id") int id, AddressR addressR)
			throws AppException {
		addressR = customerActivity.updateAddress(customerId, id, addressR);
		return Response.status(200).entity(addressR).build();
	}

	@DELETE
	@Path("/{customerId}/addresses/{id}")
	@Produces({ MediaType.TEXT_HTML })
	public Response deleteAddress(@PathParam("customerId") Long customerId, @PathParam("id") int id)
			throws AppException {
		customerActivity.deleteAddress(customerId, id);
		return Response.status(204).entity("The customer's address has been removed.").build();
	}

	@GET
	@Path("{customerId}/orders")
	public Response getOrders(@NotNull @PathParam("customerId") Long customerId) {

		List<OrderR> orders = orderActivity.getOrdersByCustomer(customerId);

		return Response.status(200).entity(orders).build();
	}

}
