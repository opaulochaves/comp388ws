package com.comp388ws.ecommerce.service;

import java.net.URI;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.comp388ws.ecommerce.errors.AppException;
import com.comp388ws.ecommerce.model.Login;
import com.comp388ws.ecommerce.representation.OrderR;
import com.comp388ws.ecommerce.representation.ProductR;
import com.comp388ws.ecommerce.representation.SellerR;
import com.comp388ws.ecommerce.representation.SellerReviewR;
import com.comp388ws.ecommerce.workflow.LoginActivity;
import com.comp388ws.ecommerce.workflow.ReviewActivity;
import com.comp388ws.ecommerce.workflow.SellerActivity;

@Component
@Path("/sellers")
public class SellerResource {

	@Autowired
	private SellerActivity sellerActivity;

	@Autowired
	private LoginActivity loginActivity;

	@Autowired
	ReviewActivity reviewActivity;

	@Context
	UriInfo uri;

	/**
	 * Creates a new seller
	 * 
	 * @param sellerLogin
	 * @return
	 */

	@POST
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public Response createSeller(SellerR sellerR) {
		sellerR = sellerActivity.create(sellerR);
		addLinksToSellerR(sellerR);
		return Response.status(Response.Status.CREATED).entity(sellerR)
				.header("Location", uri.getBaseUri().toString() + "sellers/" + sellerR.getId()).build();
	}

	/**
	 * Gets information of specified seller
	 * 
	 * @param id
	 * @return
	 * @throws AppException
	 */
	@GET
	@Path("/{id}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response getById(@PathParam("id") Long id) throws AppException {
		SellerR sellerR = sellerActivity.getById(id);
		addLinksToSellerR(sellerR);
		return Response.status(200).entity(sellerR).build();
	}

	/**
	 * Gets the list of all sellers
	 * 
	 * @return
	 */
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public List<SellerR> getSellers() {
		List<SellerR> sellerRs = sellerActivity.getAll();
		for (SellerR seller : sellerRs) {
			addLinksToSellerR(seller);
		}
		return sellerRs;
	}

	private void addLinksToSellerR(SellerR seller) {
		// link to itself
		String hrefSelf = uri.getBaseUriBuilder().path(SellerResource.class).path(Long.toString(seller.getId())).build()
				.toString();
		seller.addLink(hrefSelf, "self", "get");

		// link to products
		String hrefProduct = uri.getBaseUriBuilder().path(SellerResource.class)
				.path(SellerResource.class, "getProducts").resolveTemplate("id", seller.getId()).build().toString();
		seller.addLink(hrefProduct, "product", "get");

		// link to review
		String hrefReview = uri.getBaseUriBuilder().path(SellerResource.class).path(SellerResource.class, "getReviews")
				.resolveTemplate("id", seller.getId()).build().toString();
		seller.addLink(hrefReview, "review", "get");

		// link to orders to be fulfilled
		String hrefOrders = uri.getBaseUriBuilder().path(SellerResource.class)
				.path(SellerResource.class, "getOrdersForFulfillment").resolveTemplate("id", seller.getId()).build()
				.toString();
		seller.addLink(hrefOrders, "order", "get");
	}

	/**
	 * Gets the login of the Seller
	 * 
	 * @param sellerId
	 * @return
	 * @throws AppException
	 */
	@GET
	@Path("/{id}/login")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response getLogin(@PathParam("id") Long sellerId) throws AppException {
		SellerR sellerR = sellerActivity.getById(sellerId);
		Login login = loginActivity.getBySeller(sellerR);
		return Response.status(200).entity(login).build();
	}

	/**
	 * Displaying all products of the specified seller
	 * 
	 * @param sellerId
	 * @return
	 * @throws AppException
	 */
	@GET
	@Path("/{id}/products")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response getProducts(@PathParam("id") Long sellerId) throws AppException {
		List<ProductR> products = sellerActivity.getAllProducts(sellerId);
		return Response.status(200).entity(products).build();
	}

	/**
	 * Update the specified seller
	 * 
	 * @param sellerId
	 * @return
	 * @throws AppException
	 */
	@PUT
	@Path("/{id}")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public Response updateSeller(@PathParam("id") Long id, SellerR sellerR) throws AppException {
		sellerR.setId(id);
		sellerR = sellerActivity.udpate(sellerR);

		return Response.status(200).entity(sellerR).build();
	}

	@DELETE
	@Path("/{id}")
	@Produces({ MediaType.TEXT_HTML })
	public Response removeSeller(Long sellerId) throws AppException {
		sellerActivity.remove(sellerId);
		return Response.status(204).entity("The seller is removed").build();
	}

	/**
	 * Return one review of a given seller
	 * 
	 * @param sellerId
	 * @param reviewId
	 * @return
	 */
	@GET
	@Path("/{id}/reviews/{reviewId}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response getReview(@PathParam("id") Long sellerId, @PathParam("reviewId") Long reviewId) {

		SellerReviewR review = reviewActivity.getSellerReview(reviewId);

		return Response.status(Response.Status.OK).entity(review).build();
	}

	/**
	 * Return all reviews of a seller
	 * 
	 * @param sellerId
	 * @return
	 */
	@GET
	@Path("/{id}/reviews")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<SellerReviewR> getReviews(@PathParam("id") Long sellerId) {

		List<SellerReviewR> reviews = reviewActivity.getSellerReviews(sellerId);

		return reviews;
	}

	/**
	 * Add a review to a seller
	 * 
	 * @param sellerId
	 * @param review
	 * @return
	 * @throws AppException
	 */
	@POST
	@Path("/{id}/reviews")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.TEXT_HTML })
	public Response addReview(@PathParam("id") Long sellerId, SellerReviewR review) throws AppException {
		review.setSeller(sellerId);
		review = reviewActivity.addSellerReview(sellerId, review);
		URI reviewUri = uri.getAbsolutePathBuilder().path(review.getId().toString()).build();

		return Response.status(Response.Status.CREATED).entity("Review created successfully")
				.header("Location", reviewUri.toASCIIString()).build();
	}

	@GET
	@Path("/{id}/orders/fulfillment")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response getOrdersForFulfillment(@PathParam("id") Long sellerId) throws AppException {
		List<OrderR> orders = sellerActivity.getOrdersForFulfillment(sellerId);
		return Response.status(Response.Status.OK).entity(orders).build();
	}

	@POST
	@Path("/{sellerId}/orders/{orderId}/fulfillment/{itemId}/{trackingNumber}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response fulfillOrder(@PathParam("sellerId") Long sellerId, @PathParam("orderId") Long orderId,
			@PathParam("itemId") Long itemId, @PathParam("trackingNumber") String trackingNumber) throws AppException {
		OrderR order = sellerActivity.fulfillOrder(sellerId, orderId, itemId, trackingNumber);
		
		return Response.status(Response.Status.OK).entity(order).build();
	}
}
