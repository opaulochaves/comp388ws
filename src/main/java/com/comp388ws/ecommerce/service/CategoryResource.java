package com.comp388ws.ecommerce.service;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.comp388ws.ecommerce.errors.AppException;
import com.comp388ws.ecommerce.representation.CategoryR;
import com.comp388ws.ecommerce.workflow.CategoryActivity;

@Component
@Path("/categories")
public class CategoryResource {

	@Autowired
	private CategoryActivity categoryActivity;

	@Context
	UriInfo uri;

	@POST
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.TEXT_HTML })
	public Response createCategory(CategoryR categoryR) {

		categoryR = categoryActivity.create(categoryR);
		return Response.status(Response.Status.CREATED).entity("A new category has been created")
				.header("Location", uri.getAbsolutePath() + "/" + String.valueOf(categoryR.getId())).build();

	}

	@GET
	@Path("/{id}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response getById(@PathParam("id") Long id) throws AppException {
		CategoryR categoryR = categoryActivity.getById(id);
		return Response.status(200).entity(categoryR).build();
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public List<CategoryR> getCategories() {
		return categoryActivity.getAll();
	}

	@PUT
	@Path("/{id}")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.TEXT_HTML })
	public Response updateCategory(@PathParam("id") Long id, CategoryR categoryR) throws AppException {
		categoryR.setId(id);
		categoryR = categoryActivity.update(categoryR);

		return Response.status(Response.Status.ACCEPTED).entity("The category has been updated")
				.header("Location", uri.getAbsolutePath()).build();

	}

}
