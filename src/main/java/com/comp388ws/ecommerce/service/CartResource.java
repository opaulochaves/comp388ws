package com.comp388ws.ecommerce.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.comp388ws.ecommerce.errors.AppException;
import com.comp388ws.ecommerce.representation.CartR;
import com.comp388ws.ecommerce.representation.ItemCartR;
import com.comp388ws.ecommerce.workflow.CartActivity;

@Component
@Path("customers")
public class CartResource {

	@Autowired
	private CartActivity cartActivity;

	@Context
	UriInfo uri;

	@GET
	@Path("{id}/cart")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response getCart(@PathParam("id") Long customerId) {
		CartR cart = cartActivity.findByCustomer(customerId);

		return Response.status(200).entity(cart).build();
	}

	@POST
	@Path("{id}/cart")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public Response saveCart(@PathParam("id") Long customerId, CartR cart) {

		cart = cartActivity.saveCart(customerId, cart);

		return Response.status(200).entity(cart).build();
	}

	@PUT
	@Path("{customerId}/cart/{id}")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public Response updateItem(@PathParam("customerId") Long customerId, @PathParam("id") int id, ItemCartR item)
			throws AppException {

		CartR cart = cartActivity.updateItem(customerId, id, item);

		return Response.status(201).entity(cart).build();
	}

	@DELETE
	@Path("{customerId}/cart/{id}")
	@Produces({ MediaType.TEXT_HTML })
	public Response removeItem(@PathParam("customerId") Long customerId, @PathParam("id") int id) throws AppException {

		cartActivity.removeItem(customerId, id);

		return Response.status(201).entity("The item has been removed").build();
	}
}
