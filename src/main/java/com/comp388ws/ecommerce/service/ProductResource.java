package com.comp388ws.ecommerce.service;

import java.net.URI;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.comp388ws.ecommerce.errors.AppException;
import com.comp388ws.ecommerce.representation.ProductDetailR;
import com.comp388ws.ecommerce.representation.ProductR;
import com.comp388ws.ecommerce.representation.ProductReviewR;
import com.comp388ws.ecommerce.workflow.ProductActivity;
import com.comp388ws.ecommerce.workflow.ReviewActivity;

@Component
@Path("/products")
public class ProductResource {

	@Autowired
	private ProductActivity productActivity;

	@Autowired
	ReviewActivity reviewActivity;

	@Context
	UriInfo uri;

	/**
	 * Add a new product
	 * 
	 * @param product
	 * @return
	 */
	@POST
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public Response createProduct(ProductR productR) {
		productR = productActivity.create(productR);
		addLinksToProductR(productR);
		return Response.status(Response.Status.CREATED).entity(productR)
				.header("Location", uri.getAbsolutePath() + productR.getId().toString()).build();
	}

	/**
	 * Return a specific product given its id
	 * 
	 * @param id
	 * @return
	 * @throws AppException
	 */
	@GET
	@Path("/{id}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response getById(@PathParam("id") Long id) throws AppException {
		ProductR product = productActivity.findById(id);

		addLinksToProductR(product);

		return Response.status(200).entity(product).build();
	}

	/**
	 * 
	 * @param product
	 */
	private void addLinksToProductR(ProductR product) {
		// link to product
		String href = uri.getBaseUriBuilder().path(ProductResource.class).path(Long.toString(product.getId())).build()
				.toString();
		product.addLink(href, "self", "get");

		// link to seller
		href = uri.getBaseUriBuilder().path(SellerResource.class).path(Long.toString(product.getSeller())).build()
				.toString();
		product.addLink(href, "seller", "get");

		// links to details
		href = uri.getBaseUriBuilder().path(ProductResource.class).path(ProductResource.class, "getProductDetails")
				.resolveTemplate("id", product.getId()).build().toString();
		product.addLink(href, "detail", "get");
		
		// links to reviews
		href = uri.getBaseUriBuilder().path(ProductResource.class).path(ProductResource.class, "getReviews")
				.resolveTemplate("id", product.getId()).build().toString();
		product.addLink(href, "review", "get");
		
		// link to review
		String hrefReviewProduct =uri.getBaseUriBuilder().path(ProductResource.class).path(ProductResource.class, "addReview")
				.resolveTemplate("id", product.getId()).build().toString();
		product.addLink(hrefReviewProduct, "reviewProduct", "post");
	}

	/**
	 * Return all products
	 * 
	 * @return
	 * @throws AppException
	 */
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public List<ProductR> getProducts(@QueryParam("q") String search, @QueryParam("in") String ids) throws AppException {
		List<ProductR> products = null;

		products = productActivity.findByCriteria(search, ids);

		for (ProductR product : products) {
			addLinksToProductR(product);
		}

		return products;
	}

	@PUT
	@Path("/{id}")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.TEXT_HTML })
	public Response updateProduct(@PathParam("id") Long id, ProductR productR) {
		productR.setId(id);
		productR = productActivity.update(productR);

		return Response.status(Response.Status.OK).entity("The product has been updated.").build();
	}

	/**
	 * Add a detail to a given product
	 * 
	 * @param productId
	 * @param detail
	 * @return
	 * @throws AppException
	 */
	@POST
	@Path("/{id}/details")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.TEXT_HTML })
	public Response addProductDetail(@PathParam("id") Long productId, ProductDetailR detailR) throws AppException {
		detailR = productActivity.addProductDetail(productId, detailR);

		return Response.status(Response.Status.CREATED).entity("A new detail has been added to the product.")
				.header("Location", uri.getAbsolutePath().toString()).build();
	}

	/**
	 * Return all details of a given product
	 * 
	 * @param productId
	 * @return
	 * @throws AppException
	 */
	@GET
	@Path("/{id}/details")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<ProductDetailR> getProductDetails(@PathParam("id") Long productId) throws AppException {
		return productActivity.getProductDetails(productId);
	}

	/**
	 * Update a given detail of a given product
	 *
	 * @param productId
	 * @param detailId
	 * @param detail
	 * @return
	 * @throws AppException
	 */
	@PUT
	@Path("/{id}/details/{detailId}")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.TEXT_HTML })
	public Response updateProductDetail(@PathParam("id") Long productId, @PathParam("detailId") Long detailId,
			ProductDetailR detailR) throws AppException {
		detailR.setId(detailId);
		detailR = productActivity.updateProductDetail(productId, detailR);

		return Response.status(Response.Status.OK).entity("The product detail has been updated.").build();
	}

	/**
	 * Delete a given detail of a given product
	 *
	 * @param productId
	 * @param detailId
	 * @return
	 * @throws AppException
	 */
	@DELETE
	@Path("/{id}/details/{detailId}")
	@Produces({ MediaType.TEXT_HTML })
	public Response deleteProductDetail(@PathParam("id") Long productId, @PathParam("detailId") Long detailId)
			throws AppException {
		productActivity.deleteProductDetail(productId, detailId);

		return Response.status(Response.Status.NO_CONTENT).build();
	}

	/**
	 * Delete all details of a given product
	 *
	 * @param productId
	 * @return
	 * @throws AppException
	 */
	@DELETE
	@Path("/{id}/details")
	@Produces({ MediaType.TEXT_HTML })
	public Response deleteProductDetails(@PathParam("id") Long productId) throws AppException {
		productActivity.deleteProductDetails(productId);

		return Response.status(Response.Status.NO_CONTENT).build();
	}

	@GET
	@Path("/{id}/reviews/{reviewId}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response getReview(@PathParam("id") Long productId, @PathParam("reviewId") Long reviewId) {

		ProductReviewR review = reviewActivity.getProductReview(reviewId);

		return Response.status(Response.Status.OK).entity(review).build();
	}

	@GET
	@Path("/{id}/reviews")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<ProductReviewR> getReviews(@PathParam("id") Long productId) {

		List<ProductReviewR> reviews = reviewActivity.getProductReviews(productId);

		return reviews;
	}

	/**
	 * Adds a review to a product
	 * 
	 * @param productId
	 * @param review
	 * @return
	 * @throws AppException
	 */
	@POST
	@Path("/{id}/reviews")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public Response addReview(@PathParam("id") Long productId, ProductReviewR review) throws AppException {
		review.setProduct(productId);
		review = reviewActivity.addProductReview(productId, review);
		URI reviewUri = uri.getAbsolutePathBuilder().path(review.getId().toString()).build();

		return Response.status(Response.Status.CREATED).entity(review)
				.header("Location", reviewUri.toASCIIString()).build();
	}
}
