package com.comp388ws.ecommerce.service;

import javax.validation.constraints.NotNull;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.comp388ws.ecommerce.errors.AppException;
import com.comp388ws.ecommerce.representation.ItemCartR;
import com.comp388ws.ecommerce.representation.NewOrderR;
import com.comp388ws.ecommerce.representation.OrderR;
import com.comp388ws.ecommerce.representation.OrderStatusR;
import com.comp388ws.ecommerce.workflow.OrderActivity;

@Component
@Path("orders")
public class OrderResource {

	@Autowired
	private OrderActivity orderActivity;

	@Context
	UriInfo uri;

	@GET
	@Path("{id}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response getOrder(@PathParam("id") Long id) throws AppException {
		OrderR order = orderActivity.getById(id);
		addLinksToOrderR(order);
		return Response.status(200).entity(order).build();
	}

	/**
	 *
	 * @param orderR
	 * @return
	 * @throws AppException
	 */
	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	public Response createOrder(@NotNull NewOrderR orderR) throws AppException {
		OrderR order = orderActivity.createOrder(orderR);
		// TODO add links to new order create
		return Response.status(200).entity(order).build();
	}

	@POST
	@Path("{id}/items")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response addItem(@PathParam("id") Long id, ItemCartR item) {
		OrderR order = orderActivity.addItem(id, item);

		return Response.status(200).entity(order).build();
	}

	@POST
	@Path("{id}/payment")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response payOrder(@PathParam("id") Long id, NewOrderR orderR) {
		OrderR order = orderActivity.payOrder(id, orderR);
		return Response.status(200).entity(order).build();
	}
	
	@DELETE
	@Path("{id}/items/{productId}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response removeItem(@PathParam("id") Long id, @PathParam("productId") Long productId) {
		OrderR order = orderActivity.deleteItem(id, productId);
		return Response.status(200).entity(order).build();
	}

	/**
	 * 
	 * @param id
	 * @return
	 * @throws AppException
	 */
	/*@GET
	@Path("{customerId}/orders")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<OrderR> getOrderByCustomer(@PathParam("customerId") Long customerId) throws AppException {
		List<OrderR> orders = orderActivity.getOrdersByCustomer(customerId);
		for (OrderR order : orders) {
			addLinksToOrderR(order);
		}
		return orders;
	}*/

	/**
	 * 
	 * @param id
	 * @return
	 * @throws AppException
	 */
	/*@GET
	@Path("{customerId}/orders/{id}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response getById(@PathParam("customerId") Long customerId, @PathParam("id") Long id) throws AppException {
		OrderR order = orderActivity.getById(id);
		addLinksToOrderR(order);
		return Response.status(200).entity(order).build();
	}*/

	private void addLinksToOrderR(OrderR order) {
		// link to itself
		String hrefSelf = uri.getBaseUriBuilder().path(OrderResource.class).path(OrderResource.class, "getOrder")
				.resolveTemplate("id", order.getId()).build().toString();
		order.addLink(hrefSelf, "self", "get");

		// link to pay
		String hrefPay = uri.getBaseUriBuilder().path(OrderResource.class).path(OrderResource.class, "payOrder")
				.resolveTemplate("id", order.getId()).build().toString();
		order.addLink(hrefPay, "order pay", "post");

		// link to cancel
		String hrefCancel = uri.getBaseUriBuilder().path(OrderResource.class).path(OrderResource.class, "cancelOrder")
				.resolveTemplate("id", order.getId().toString()).build().toString();
		order.addLink(hrefCancel, "order cancel", "delete");

		// link to status
		String hrefStatus = uri.getBaseUriBuilder().path(OrderResource.class).path(OrderResource.class, "getStatus")
				.resolveTemplate("id", order.getId().toString()).build().toString();
		order.addLink(hrefStatus, "order status", "get");
	}

	/**
	 * Creates an order based on the cart and on the representation which only
	 * contains the shipping address
	 * 
	 * @param customerId
	 * @param order
	 * @return
	 * @throws AppException
	 */
	/*@POST
	@Path("{customerId}/orders")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.TEXT_HTML })
	public Response createOrder(@PathParam("customerId") Long customerId, OrderR order) throws AppException {
		order = orderActivity.create(order, customerId);

		return Response.status(201).entity("A new order has been created")
				.header("Location", uri.getAbsolutePath() + "/" + order.getId()).build();
	}*/

	/**
	 * Make a payment for an order
	 * 
	 * @param customerId
	 * @param id
	 * @return
	 * @throws AppException
	 */
	/*@POST
	@Path("{customerId}/orders/{id}/payment")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response makePayment(@PathParam("customerId") Long customerId, @PathParam("id") Long id)
			throws AppException {
		OrderR order = orderActivity.makePayment(id);

		return Response.status(200).entity(order).build();
	}*/

	/**
	 * Cancel a given order
	 * 
	 * @param customerId
	 * @param id
	 * @return
	 * @throws AppException
	 */
	@DELETE
	@Path("{id}")
	@Produces(MediaType.TEXT_HTML)
	public Response cancelOrder(@PathParam("id") Long id)
			throws AppException {
		orderActivity.cancel(id);

		return Response.status(200).entity("The order #" + id + " has been cancelled.").build();
	}

	/**
	 * Returns the current status of an order
	 * 
	 * @param customerId
	 * @param id
	 * @return
	 * @throws AppException
	 */
	@GET
	@Path("{id}/status")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getStatus(@PathParam("id") Long id) throws AppException {
		OrderStatusR status = orderActivity.getOrderStatus(id);

		return Response.status(200).entity(status).build();
	}
}
