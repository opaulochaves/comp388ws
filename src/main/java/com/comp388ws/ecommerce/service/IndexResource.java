package com.comp388ws.ecommerce.service;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.comp388ws.ecommerce.errors.AppException;
import com.comp388ws.ecommerce.model.Login;
import com.comp388ws.ecommerce.representation.IndexR;
import com.comp388ws.ecommerce.representation.UserR;
import com.comp388ws.ecommerce.workflow.LoginActivity;

@Component
@Path("/")
public class IndexResource {

	@Autowired
	private LoginActivity loginActivity;

	@Context
	UriInfo uri;

	@GET
	@Path("swagger.json")
	@Produces({ MediaType.APPLICATION_JSON })
	public String getSwagger() throws IOException {
		String jsonContent = "";
		// http://stackoverflow.com/questions/4716503/best-way-to-read-a-text-file-in-java
		BufferedReader br = new BufferedReader(new FileReader("swagger.json"));
		try {
			StringBuilder sb = new StringBuilder();
			String line = br.readLine();

			while (line != null) {
				sb.append(line);
				sb.append(System.lineSeparator());
				line = br.readLine();
			}

			jsonContent = sb.toString();
		} finally {
			br.close();
		}

		return jsonContent;
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public Response get() {
		IndexR index = new IndexR();
		addLinks(index);
		return Response.status(200).entity(index).build();
	}

	@POST
	@Path("login")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response login(@NotNull UserR user) {

		Login login = loginActivity.getByEmail(user.getEmail());
		if (login == null) {
			throw new AppException(401, "Email or password did not match");
		}
		user = new UserR(login);

		return Response.status(200).entity(user).build();
	}

	private void addLinks(IndexR index) {
		String href = uri.getBaseUriBuilder().path(ProductResource.class).build().toString();
		index.addLink(href, "product", "get");

		href = uri.getBaseUriBuilder().path(CustomerResource.class).build().toString();
		index.addLink(href, "customer", "get");

		href = uri.getBaseUriBuilder().path(SellerResource.class).build().toString();
		index.addLink(href, "seller", "get");
	}
}
