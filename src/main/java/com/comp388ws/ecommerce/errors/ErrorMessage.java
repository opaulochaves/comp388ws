package com.comp388ws.ecommerce.errors;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Response;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * http://www.codingpedia.org/ama/error-handling-in-rest-api-with-jersey/
 * https://github.com/Codingpedia/demo-rest-jersey-spring
 *
 */
@XmlRootElement
public class ErrorMessage {

	/** contains the same HTTP Status code returned by the server */
	@XmlElement(name = "status")
	int status;

	/** message describing the error */
	@XmlElement(name = "message")
	String message;
	
	@XmlElement(name = "dev_message")
	String devMessage;

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public ErrorMessage(AppException ex) {
		this.status = ex.getStatus();
		this.message = ex.getMessage();
	}

	public ErrorMessage(NotFoundException ex) {
		this.status = Response.Status.NOT_FOUND.getStatusCode();
		this.message = ex.getMessage();
	}

	public ErrorMessage() {
	}

	public String getDevMessage() {
		return devMessage;
	}

	public void setDevMessage(String devMessage) {
		this.devMessage = devMessage;
	}
}