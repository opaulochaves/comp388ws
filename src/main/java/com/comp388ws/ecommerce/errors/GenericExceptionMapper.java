package com.comp388ws.ecommerce.errors;

import java.io.PrintWriter;
import java.io.StringWriter;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * http://www.codingpedia.org/ama/error-handling-in-rest-api-with-jersey/
 * https://github.com/Codingpedia/demo-rest-jersey-spring
 */
@Provider
public class GenericExceptionMapper implements ExceptionMapper<Throwable> {

	private static final Logger logger = LoggerFactory.getLogger(GenericExceptionMapper.class);

	public Response toResponse(Throwable ex) {
		ErrorMessage errorMessage = new ErrorMessage();

		errorMessage.setMessage(ex.getMessage());

		setHttpStatus(ex, errorMessage);

		logger.error(errorMessage.getMessage(), ex);
		return Response.status(errorMessage.getStatus()).entity(errorMessage).type(MediaType.APPLICATION_JSON).build();
	}

	private void setHttpStatus(Throwable ex, ErrorMessage errorMessage) {
		if (ex instanceof AppException) {
			errorMessage.setStatus(((AppException) ex).getStatus());
		} else if (ex instanceof WebApplicationException) {
			// NICE way to combine both of methods, say it in the blog
			errorMessage.setStatus(((WebApplicationException) ex).getResponse().getStatus());
		} else {
			// TODO check if it's development mode
			StringWriter errorStackTrace = new StringWriter();
			ex.printStackTrace(new PrintWriter(errorStackTrace));
			errorMessage.setDevMessage(errorStackTrace.toString());
			// defaults to internal server error 500
			errorMessage.setStatus(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode());
		}
	}
}