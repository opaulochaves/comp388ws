package com.comp388ws.ecommerce.errors;

/**
 * Class to map application related exceptions
 * http://www.codingpedia.org/ama/error-handling-in-rest-api-with-jersey/
 * https://github.com/Codingpedia/demo-rest-jersey-spring
 * 
 * @author amacoder
 *
 */
public class AppException extends RuntimeException {

	private static final long serialVersionUID = -1072761862893384051L;
	/**
	 * contains redundantly the HTTP status of the response sent back to the
	 * client in case of error, so that the developer does not have to look into
	 * the response headers. If null a default
	 */
	Integer status;

	/**
	 * 
	 * @param status
	 * @param code
	 * @param message
	 * @param developerMessage
	 * @param link
	 */
	public AppException(int status, String message) {
		super(message);
		this.status = status;
	}

	public AppException() {
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

}