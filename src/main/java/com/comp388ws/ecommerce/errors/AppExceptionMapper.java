package com.comp388ws.ecommerce.errors;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * http://www.codingpedia.org/ama/error-handling-in-rest-api-with-jersey/
 * https://github.com/Codingpedia/demo-rest-jersey-spring
 * 
 * TODO this class is not being called
 */
@Provider
public class AppExceptionMapper implements ExceptionMapper<AppException> {

	public Response toResponse(AppException ex) {
		return Response.status(ex.getStatus()).entity(new ErrorMessage(ex)).type(MediaType.APPLICATION_JSON).build();
	}

}