package com.comp388ws.ecommerce.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;

@Entity
@Table(name = "login")
public class Login {

	@Id
	@GeneratedValue
	private Long id;

	// TODO Make username option (not used anymore, only email is enough)
	@Column(nullable = false)
	private String username;

	@Column(unique = true, nullable = false)
	private String email;

	@Column(nullable = false)
	private String password;

	@Column(nullable = false)
	private Boolean active;

	@Column
	private Boolean admin;

	@Column(name = "created_at")
	private LocalDateTime createdAt;

	@Column(name = "last_access")
	private LocalDateTime lastAccess;

	@Column
	private String token;

	@OneToOne
	@JoinColumn(name = "customers_id")
	private Customer customer;

	@OneToOne
	@JoinColumn(name = "sellers_id")
	private Seller seller;

	public Login() {
	}

	/**
	 * Create a login for a customer
	 * 
	 * @param customer
	 * @param username
	 * @param password
	 */
	public Login(Customer customer, String username, String password) {
		this.setCustomer(customer);
		this.setUsername(username);
		this.setEmail(customer.getEmail());
		this.setPassword(password);
	}

	/**
	 * Create a login for a seller
	 * 
	 * @param seller
	 * @param username
	 * @param password
	 */
	public Login(Seller seller, String username, String password) {
		this.setSeller(seller);
		this.setUsername(username);
		this.setEmail(seller.getEmail());
		this.setPassword(password);
	}

	@PrePersist
	private void prePersist() {
		this.setActive(true);
		this.setAdmin(false);
		this.createdAt = LocalDateTime.now();
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Boolean getAdmin() {
		return admin;
	}

	public void setAdmin(Boolean admin) {
		this.admin = admin;
	}

	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	public LocalDateTime getLastAccess() {
		return lastAccess;
	}

	public void setLastAccess(LocalDateTime lastAccess) {
		this.lastAccess = lastAccess;
	}

	public Long getId() {
		return id;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Seller getSeller() {
		return seller;
	}

	public void setSeller(Seller seller) {
		this.seller = seller;
	}
}
