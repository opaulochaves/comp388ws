package com.comp388ws.ecommerce.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.comp388ws.ecommerce.representation.AddressR;

/**
 * 
 * @author Onontsatsal
 *
 */
@Entity
@Table(name = "addresses")
public class Address {

	@Id
	@GeneratedValue
	private Long id;

	@Column(nullable = false)
	private String type;

	@Column(nullable = false)
	private String line1;

	@Column
	private String line2;

	@Column
	private String city;

	@Column(length = 2)
	private String state;

	@Column(length = 5, nullable = false)
	private String zipcode;

	@Column(length = 2)
	private String country;

	public Address() {
	}

	public Address(AddressR addressR) {
		this.setId(addressR.getId());
		this.setType(addressR.getType());
		this.setLine1(addressR.getLine1());
		this.setLine2(addressR.getLine2());
		this.setCity(addressR.getCity());
		this.setState(addressR.getState());
		this.setZipcode(addressR.getZipcode());
		this.setCountry(addressR.getCountry());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getLine1() {
		return line1;
	}

	public void setLine1(String line1) {
		this.line1 = line1;
	}

	public String getLine2() {
		return line2;
	}

	public void setLine2(String line2) {
		this.line2 = line2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

}
