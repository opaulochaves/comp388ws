package com.comp388ws.ecommerce.model;

/**
 * http://blog.chris-ritchie.com/2013/09/mapping-enums-with-fixed-id-in-jpa.html
 * 
 * @author pchavesdasilvafilho
 *
 */
public enum Status {

	IN_PROGRESS(1), PAID(2), SHIPPED(3), DELIVERED(4), CANCELLED(5);

	private int id;

	private Status(int id) {
		this.id = id;
	}

	public static Status getType(Integer id) {
		if (id == null) {
			return null;
		}

		for (Status status : Status.values()) {
			if (id.equals(status.getId())) {
				return status;
			}
		}

		throw new IllegalArgumentException("No matching type for id " + id);
	}

	public int getId() {
		return id;
	}
}
