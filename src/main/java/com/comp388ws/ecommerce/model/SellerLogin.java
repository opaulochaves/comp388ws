package com.comp388ws.ecommerce.model;

public class SellerLogin {

	private Seller seller;

	private Login login;

	public Seller getSeller() {
		return seller;
	}

	public void setSeller(Seller seller) {
		this.seller = seller;
	}

	public Login getLogin() {
		return login;
	}

	public void setLogin(Login login) {
		this.login = login;
	}

}
