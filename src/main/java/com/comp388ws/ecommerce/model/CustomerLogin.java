package com.comp388ws.ecommerce.model;

public class CustomerLogin {

	private Customer customer;

	private Login login;

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Login getLogin() {
		return login;
	}

	public void setLogin(Login login) {
		this.login = login;
	}

}
