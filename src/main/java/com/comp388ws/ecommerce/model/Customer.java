package com.comp388ws.ecommerce.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Email;

import com.comp388ws.ecommerce.representation.CustomerR;

/**
 * 
 * @author paulo
 *
 */
@Entity
@Table(name = "customers")
public class Customer {

	@DecimalMin(value = "1")
	@Id
	@GeneratedValue
	private Long id;

	@NotNull(message = "{customer.firstName.empty}")
	@Column(name = "first_name", nullable = false)
	private String firstName;

	@NotNull(message = "{customer.lastName.empty}")
	@Column(name = "last_name", nullable = false)
	private String lastName;

	@NotNull
	@Email(message = "{customer.wrong.email}", regexp = "[a-zA-Z0-9._%-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}")
	@Column(unique = true, nullable = false)
	private String email;

	@Pattern(message = "{customer.phone.wrong}", regexp = "[0-9]{3,10}")
	@Column(length = 15)
	private String phone;

	@OneToMany(mappedBy = "customer")
	private List<CreditCard> creditCards;

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "customers_id")
	private List<Address> addresses = new ArrayList<>();

	@Column(nullable = false)
	private Boolean active;

	@OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "cart_id")
	private Cart cart;

	@OneToMany(mappedBy = "customer")
	private List<Review> reviews = new ArrayList<>();

	public Customer() {
	}

	public Customer(CustomerR customerR) {
		this.setId(customerR.getId());
		this.setFirstName(customerR.getFirstName());
		this.setLastName(customerR.getLastName());
		this.setEmail(customerR.getEmail());
		this.setPhone(customerR.getPhone());
		this.setActive(true);
	}

	@PrePersist
	private void prePersist() {
		this.setActive(true);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public List<CreditCard> getCreditCards() {
		return creditCards;
	}

	public void addCreditCard(CreditCard creditCard) {
		creditCards.add(creditCard);
	}

	public List<Address> getAddresses() {
		return addresses;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public List<Review> getReviews() {
		return reviews;
	}

	public Cart getCart() {
		return cart;
	}

	public void setCart(Cart cart) {
		this.cart = cart;
	}

}
