package com.comp388ws.ecommerce.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 
 * @author Onontsatsal
 *
 */
@Entity
@Table(name = "order_products")
public class OrderProduct {

	@Id
	@GeneratedValue
	private Long id;

	@Column(nullable = false)
	private int quantity;

	@Column(name = "unit_price", precision = 10, scale = 4, columnDefinition = "DECIMAL(10,4)", nullable = false)
	private BigDecimal unitPrice;

	@Column(name = "tracking_number", length = 10)
	private String trackingNumber;

	@ManyToOne
	@JoinColumn(name = "products_id", nullable = false)
	private Product product;
	
	@ManyToOne
	@JoinColumn(name = "orders_id", nullable = false)
	private Order order;

	// 1-Cancelled, 2-Fulfillment, 3-Shipped, 4-Delivered
	@Column
	private String status;

	public OrderProduct() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}

	public String getTrackingNumber() {
		return trackingNumber;
	}

	public void setTrackingNumber(String trackingNumber) {
		this.trackingNumber = trackingNumber;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

}
