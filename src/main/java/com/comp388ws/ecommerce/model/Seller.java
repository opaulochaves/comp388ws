package com.comp388ws.ecommerce.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.Pattern;

import com.comp388ws.ecommerce.representation.SellerR;

/**
 * 
 * @author Onontsatsal
 *
 */
@Entity
@Table(name = "sellers")
public class Seller {

	@Id
	@GeneratedValue
	private Long id;

	@Column(name = "first_name", nullable = false)
	private String firstName;

	@Column(name = "last_name", nullable = false)
	private String lastName;

	@Column
	private String companyName;

	@Column(unique = true, nullable = false)
	private String email;

	@Pattern(message = "{seller.phone.wrong}", regexp = "[0-9]{8,10}")
	@Column(length = 15)
	private String phone;

	@Column(name = "sellers_type", length = 15, nullable = false)
	private String type;

	@OneToMany(mappedBy = "seller")
	private List<Product> products;

	@OneToMany(mappedBy = "seller")
	private List<Review> reviews = new ArrayList<>();

	@Column(nullable = false)
	private Boolean active;

	public Seller() {
	}

	public Seller(SellerR sellerR) {
		this.setId(sellerR.getId());
		this.setFirstName(sellerR.getFirstName());
		this.setLastName(sellerR.getLastName());
		this.setEmail(sellerR.getEmail());
		this.setPhone(sellerR.getPhone());
		this.setCompanyName(sellerR.getCompanyName());
		this.setType(sellerR.getType());
	}

	@PrePersist
	private void prePersist() {
		this.setActive(true);
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public List<Review> getReviews() {
		return reviews;
	}

	public List<Product> getProducts() {
		return products;
	}

}
