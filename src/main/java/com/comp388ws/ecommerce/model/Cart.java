package com.comp388ws.ecommerce.model;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.comp388ws.ecommerce.representation.CartR;

@Entity
@Table(name = "cart")
public class Cart {

	@Id
	@GeneratedValue
	private Long id;

	@Column(name = "created_at")
	private LocalDateTime createdAd;

	@OneToMany(mappedBy = "cart", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<ItemCart> items = new ArrayList<>();

	public Cart() {
		this.setCreatedAd(LocalDateTime.now());
	}

	public Cart(CartR cartR) {
		this.setCreatedAd(cartR.getCreatedAd());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDateTime getCreatedAd() {
		return createdAd;
	}

	public void setCreatedAd(LocalDateTime createdAd) {
		this.createdAd = createdAd;
	}

	public List<ItemCart> getItems() {
		return items;
	}

	public void setItems(List<ItemCart> items) {
		this.items = items;
	}

}
