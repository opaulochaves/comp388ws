package com.comp388ws.ecommerce.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.comp388ws.ecommerce.representation.ProductDetailR;

/**
 * 
 * @author Onontsatsal
 *
 */
@Entity
@Table(name = "product_details")
public class ProductDetail {

	@Id
	@GeneratedValue
	private Long id;

	@Column(name = "product_key", nullable = false)
	private String key;

	@Column(name = "product_value", nullable = false)
	private String value;

	public ProductDetail() {
	}

	public ProductDetail(ProductDetailR productDetailR) {
		this.setId(productDetailR.getId());
		this.setKey(productDetailR.getKey());
		this.setValue(productDetailR.getValue());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
