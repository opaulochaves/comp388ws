package com.comp388ws.ecommerce.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * 
 * @author Onontsatsal
 *
 */
@Entity
@Table(name = "orders")
public class Order {

	@Id
	@GeneratedValue
	private Long id;

	@Column(length = 20)
	private String number;

	@Column(name = "tracking_number", length = 10)
	private String trackingNumber;

	@JoinColumn(name = "customers_id", nullable = false)
	@ManyToOne(fetch = FetchType.LAZY)
	private Customer customer;

	@Column(name = "order_placed_at", nullable = false)
	private LocalDateTime orderPlacedAt;

	@Column(name = "total_price", precision = 10, scale = 4, columnDefinition = "DECIMAL(10,4)")
	private BigDecimal totalPrice;

	@Column(name = "order_paid_at")
	private LocalDateTime orderPaidAt;

	@OneToMany(mappedBy = "order", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
	private List<OrderStatus> status = new ArrayList<>();

	@OneToMany(mappedBy = "order", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
	private List<OrderProduct> items = new ArrayList<>();

	@ManyToOne
	@JoinColumn(name = "credit_cards_id")
	private CreditCard creditCard;

	@OneToOne
	@JoinColumn(name = "shipping_address_id")
	private Address shippingAddress;

	public Order() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getTrackingNumber() {
		return trackingNumber;
	}

	public void setTrackingNumber(String trackingNumber) {
		this.trackingNumber = trackingNumber;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public LocalDateTime getOrderPlacedAt() {
		return orderPlacedAt;
	}

	public void setOrderPlacedAt(LocalDateTime orderPlacedAt) {
		this.orderPlacedAt = orderPlacedAt;
	}

	public BigDecimal getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(BigDecimal totalPrice) {
		this.totalPrice = totalPrice;
	}

	public CreditCard getCreditCard() {
		return creditCard;
	}

	public void setCreditCard(CreditCard creditCard) {
		this.creditCard = creditCard;
	}

	public LocalDateTime getOrderPaidAt() {
		return orderPaidAt;
	}

	public void setOrderPaidAt(LocalDateTime orderPaidAt) {
		this.orderPaidAt = orderPaidAt;
	}

	public List<OrderStatus> getStatus() {
		return status;
	}

	public List<OrderProduct> getItems() {
		return items;
	}

	public void setItems(List<OrderProduct> items) {
		this.items = items;
	}

	public void addItem(OrderProduct item) {
		this.items.add(item);
	}

	public Address getShippingAddress() {
		return shippingAddress;
	}

	public void setShippingAddress(Address shippingAddress) {
		this.shippingAddress = shippingAddress;
	}
}
