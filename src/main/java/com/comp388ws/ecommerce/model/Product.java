package com.comp388ws.ecommerce.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;

import com.comp388ws.ecommerce.representation.ProductDetailR;
import com.comp388ws.ecommerce.representation.ProductR;

/**
 * 
 * @author Onontsatsal
 *
 */
@Entity
@Table(name = "products")
public class Product {

	@Id
	@GeneratedValue
	private Long id;

	@Column(nullable = false)
	private String name;

	@Column(nullable = false, length = 512)
	private String description;

	@Column(name = "unit_price", precision = 10, scale = 4, columnDefinition = "DECIMAL(10,4)", nullable = false)
	private BigDecimal unitPrice;

	@Column(nullable = false)
	private Long quantity;

	@Column(name = "brand_name")
	private String brandName;

	@Column(name = "available_from")
	private LocalDateTime availableFrom;

	@Column(name = "created_at")
	private LocalDateTime createdAt;

	@ManyToOne
	@JoinColumn(name = "categories_id", nullable = false)
	private Category category;

	@ManyToOne
	@JoinColumn(name = "sellers_id")
	private Seller seller;

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "products_id")
	private List<ProductDetail> details = new ArrayList<>();

	@OneToMany(mappedBy = "product")
	private List<Review> reviews = new ArrayList<>();

	@Column(nullable = false)
	private Boolean active;

	public Product() {
	}

	public Product(ProductR productR) {
		this.setId(productR.getId());
		this.setName(productR.getName());
		this.setDescription(productR.getDescription());
		this.setUnitPrice(productR.getUnitPrice());
		this.setQuantity(productR.getQuantity());
		this.setBrandName(productR.getBrandName());
		this.setAvailableFrom(productR.getAvailableFrom());

		Category category = new Category();
		Seller seller = new Seller();

		category.setId(productR.getCategory());
		seller.setId(productR.getSeller());

		this.setCategory(category);
		this.setSeller(seller);

		for (ProductDetailR detail : productR.getDetails()) {
			this.addDetail(new ProductDetail(detail));
		}
	}

	@PrePersist
	public void prePersit() {
		createdAt = LocalDateTime.now();
		this.setActive(true);
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public BigDecimal getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}

	public Long getQuantity() {
		return quantity;
	}

	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public LocalDateTime getAvailableFrom() {
		return availableFrom;
	}

	public void setAvailableFrom(LocalDateTime availableFrom) {
		this.availableFrom = availableFrom;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}

	public void setSeller(Seller seller) {
		this.seller = seller;
	}

	public void setDetails(List<ProductDetail> details) {
		this.details = details;
	}

	public void addDetail(ProductDetail detail) {
		this.getDetails().add(detail);
	}

	public Seller getSeller() {
		return seller;
	}

	public List<ProductDetail> getDetails() {
		return details;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean isActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public List<Review> getReviews() {
		return reviews;
	}

}
