package com.comp388ws.ecommerce.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.comp388ws.ecommerce.util.LocalDateTimeXmlAdapter;

/**
 * 
 * @author Onontsatsal
 *
 */
@Entity
@Table(name = "orders_status")
public class OrderStatus {

	@Id
	@GeneratedValue
	@XmlTransient
	private Long id;

	@Column(nullable = false)
	private Integer status;

	@Column(name = "updated_at")
	@XmlElement(name = "updated_at")
	@XmlJavaTypeAdapter(value = LocalDateTimeXmlAdapter.class)
	private LocalDateTime updatedAt;

	@ManyToOne
	@JoinColumn(name = "orders_id", nullable = false)
	private Order order;

	public OrderStatus() {
	}

	public OrderStatus(Status status) {
		this.setStatus(status);
		this.setUpdatedAt(LocalDateTime.now());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Order getOrder() {
		return order;
	}
	
	public void setOrder(Order order) {
		this.order = order;
	}

	public Status getStatus() {
		return Status.getType(this.status);
	}

	public void setStatus(Status status) {
		if (status == null) {
			this.status = null;
		} else {
			this.status = status.getId();
		}
	}

	public LocalDateTime getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(LocalDateTime updatedAt) {
		this.updatedAt = updatedAt;
	}

}
