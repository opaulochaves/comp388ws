package com.comp388ws.ecommerce.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;

import com.comp388ws.ecommerce.representation.CreditCardR;

/**
 * 
 * @author Onontsatsal
 *
 */
@Entity
@Table(name = "credit_cards")
public class CreditCard {

	@Id
	@GeneratedValue
	private Long id;

	@Column(name = "card_type", nullable = false)
	private String cardType;

	@Column(name = "card_number", length = 16, nullable = false)
	private String cardNumber;

	@Column(name = "security_code", length = 3, nullable = false)
	private String securityCode;

	@Column(name = "name_on_card", nullable = false)
	private String nameOnCard;

	@Column(name = "exp_month", nullable = false)
	private int expMonth;

	@Column(name = "exp_year", nullable = false)
	private int expYear;

	@Column
	private boolean active;

	@ManyToOne
	@JoinColumn(name = "customers_id")
	private Customer customer;

	public CreditCard() {
	}

	public CreditCard(CreditCardR creditCardR) {
		this.setId(creditCardR.getId());
		this.setCardType(creditCardR.getCardType());
		this.setCardNumber(creditCardR.getCardNumber());
		this.setNameOnCard(creditCardR.getNameOnCard());
		this.setSecurityCode(creditCardR.getSecurityCode());
		this.setExpMonth(creditCardR.getExpMonth());
		this.setExpYear(creditCardR.getExpYear());
	}

	@PrePersist
	public void prePersist() {
		this.setActive(true);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getSecurityCode() {
		return securityCode;
	}

	public void setSecurityCode(String securityCode) {
		this.securityCode = securityCode;
	}

	public String getNameOnCard() {
		return nameOnCard;
	}

	public void setNameOnCard(String nameOnCard) {
		this.nameOnCard = nameOnCard;
	}

	public int getExpMonth() {
		return expMonth;
	}

	public void setExpMonth(int expMonth) {
		this.expMonth = expMonth;
	}

	public int getExpYear() {
		return expYear;
	}

	public void setExpYear(int expYear) {
		this.expYear = expYear;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

}
