package com.comp388ws.ecommerce.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.comp388ws.ecommerce.representation.ProductReviewR;
import com.comp388ws.ecommerce.representation.SellerReviewR;

@Entity
@Table(name = "reviews")
public class Review {

	@Id
	@GeneratedValue
	private Long id;

	@Column(nullable = false)
	private String title;

	@Column(nullable = false, length=512)
	private String body;

	@Column(nullable = false)
	private int rating;

	@Column(name = "created_at", nullable = false)
	private LocalDateTime createdAt;

	@ManyToOne
	@JoinColumn(name = "customers_id", nullable = false)
	private Customer customer;

	@ManyToOne
	@JoinColumn(name = "products_id")
	private Product product;

	@ManyToOne
	@JoinColumn(name = "sellers_id")
	private Seller seller;

	public Review() {
	}

	public Review(SellerReviewR reviewR, Seller seller, Customer customer) {
		setId(reviewR.getId());
		setTitle(reviewR.getTitle());
		setBody(reviewR.getBody());
		setRating(reviewR.getRating());
		setCustomer(customer);
		setSeller(seller);

		if (reviewR.getCreatedAt() == null) {
			setCreatedAt(LocalDateTime.now());
		} else {
			setCreatedAt(reviewR.getCreatedAt());
		}
	}

	public Review(ProductReviewR reviewR, Product product, Customer customer) {
		setId(reviewR.getId());
		setTitle(reviewR.getTitle());
		setBody(reviewR.getBody());
		setRating(reviewR.getRating());
		setCustomer(customer);
		setProduct(product);

		if (reviewR.getCreatedAt() == null) {
			setCreatedAt(LocalDateTime.now());
		} else {
			setCreatedAt(reviewR.getCreatedAt());
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Seller getSeller() {
		return seller;
	}

	public void setSeller(Seller seller) {
		this.seller = seller;
	}

}
