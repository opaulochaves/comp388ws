package com.comp388ws.ecommerce.workflow;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.comp388ws.ecommerce.errors.AppException;
import com.comp388ws.ecommerce.model.Address;
import com.comp388ws.ecommerce.model.Cart;
import com.comp388ws.ecommerce.model.CreditCard;
import com.comp388ws.ecommerce.model.Customer;
import com.comp388ws.ecommerce.model.ItemCart;
import com.comp388ws.ecommerce.model.Order;
import com.comp388ws.ecommerce.model.OrderProduct;
import com.comp388ws.ecommerce.model.OrderStatus;
import com.comp388ws.ecommerce.model.Product;
import com.comp388ws.ecommerce.model.Status;
import com.comp388ws.ecommerce.repositories.CustomerRepository;
import com.comp388ws.ecommerce.repositories.OrderRepository;
import com.comp388ws.ecommerce.repositories.ProductRepository;
import com.comp388ws.ecommerce.representation.ItemCartR;
import com.comp388ws.ecommerce.representation.NewOrderR;
import com.comp388ws.ecommerce.representation.OrderR;
import com.comp388ws.ecommerce.representation.OrderStatusR;

@Service("orderActivity")
@Transactional
public class OrderActivity {

	private OrderRepository orderRepositoy;
	private CustomerRepository customerRepository;
	private ProductRepository productRepository;

	@Autowired
	public OrderActivity(OrderRepository orderRepositoy, CustomerRepository customerRepository,
			ProductRepository productRepository) {
		this.orderRepositoy = orderRepositoy;
		this.customerRepository = customerRepository;
		this.productRepository = productRepository;
	}

	/**
	 * Returns an order given its id
	 * 
	 * @param id
	 * @return
	 */
	public OrderR getById(Long id) {
		Order order = orderRepositoy.findOne(id);
		if (order == null) {
			throw new AppException(404, "Order not found");
		}
		return new OrderR(order);
	}

	/**
	 * Returns a list of orders of a customer
	 * 
	 * @param customerId
	 * @return
	 */
	public List<OrderR> getOrdersByCustomer(Long customerId) {
		Customer customer = new Customer();
		customer.setId(customerId);
		Iterator<Order> orders = orderRepositoy.findByCustomer(customer).iterator();

		List<OrderR> ordersR = new ArrayList<>();
		while (orders.hasNext()) {
			ordersR.add(new OrderR(orders.next()));
		}

		return ordersR;
	}

	public OrderR createOrder(NewOrderR orderR) {
		Order order = new Order();
		List<ItemCartR> items = orderR.getItems();
		Address shippingAddress = null;
		Long addressId = orderR.getShippingAddress() != null ? orderR.getShippingAddress() : 0l;
		Customer customer = null;

		if (items == null || items.isEmpty()) {
			throw new AppException(400, "There is no item to add to the order");
		}

		customer = customerRepository.findOne(orderR.getCustomer());

		if (customer == null) {
			throw new AppException(400, "Customer not found");
		} else {
			shippingAddress = getAddressById(customer, addressId);
			if (shippingAddress == null) {
				throw new AppException(400, "Address not found");
			}
		}

		// Delete all orders in progress
		List<Order> orders = orderRepositoy.findOrderInProgress(customer);
		for (Order or : orders) {
			orderRepositoy.delete(or);
		}

		order.setCustomer(customer);
		// TODO set shipping address when paying, payOrder
		order.setShippingAddress(shippingAddress);
		order.setOrderPlacedAt(LocalDateTime.now());

		order = orderRepositoy.save(order);

		addItems(items, order);

		OrderStatus status = new OrderStatus(Status.IN_PROGRESS);
		status.setOrder(order);
		order.getStatus().add(status);

		return new OrderR(order);
	}

	/**
	 * Adds a new item to an order. If the item is already in the order, change
	 * its quantity
	 * 
	 * @param id
	 * @param item
	 * @return
	 */
	public OrderR addItem(Long id, ItemCartR item) {
		Order order = orderRepositoy.findOne(id);

		if (order == null) {
			throw new AppException(404, "No such order");
		}

		if (!isOrderInProgress(order)) {
			throw new AppException(400, "This order has already been paid, cancelled or shipped.");
		}

		addOrChangeItem(item, order);

		return new OrderR(order);
	}

	public OrderR payOrder(Long id, NewOrderR orderR) {
		Order order = orderRepositoy.findOne(id);
		CreditCard cCard = new CreditCard();
		cCard.setId(orderR.getCreditCard());

		if (order == null) {
			throw new AppException(404, "No such order");
		}
		// TODO check if order is in progress

		if (!isOrderInProgress(order)) {
			throw new AppException(400, "This order has already been paid, cancelled or shipped.");
		}

		Customer customer = order.getCustomer();

		if (cCard.getId() == null) {
			cCard = getCreditCard(customer);
		} else {
			cCard = getCreditCard(customer, cCard.getId());
		}

		order.setCreditCard(cCard);
		order.setOrderPaidAt(LocalDateTime.now());
		order.setTotalPrice(getTotalPrice(order.getItems()));

		OrderStatus status = new OrderStatus(Status.PAID);
		status.setOrder(order);
		order.getStatus().add(status);

		updateItemsStatus(order.getItems());

		order = orderRepositoy.save(order);

		return new OrderR(order);
	}

	private Boolean isOrderInProgress(Order order) {
		OrderStatus status = getLastOrderStatus(order);
		if (status.getStatus().getId() == Status.IN_PROGRESS.getId()) {
			return true;
		}
		return false;
	}

	private void addOrChangeItem(ItemCartR itemR, Order order) {
		for (OrderProduct itemOrder : order.getItems()) {
			if (itemOrder.getProduct().getId() == itemR.getProduct()) {
				itemOrder.setQuantity(itemOrder.getQuantity() + itemR.getQuantity());
				return;
			}
		}

		List<ItemCartR> items = new ArrayList<>();
		items.add(itemR);
		addItems(items, order);
	}

	/**
	 * Add items to an order. Copy all items from a cart
	 * 
	 * @param listItemsCart
	 * @param order
	 * @return
	 */
	private void addItems(List<ItemCartR> listItemsCart, Order order) {
		for (ItemCartR itemCartR : listItemsCart) {
			ItemCart itemCart = new ItemCart(itemCartR);

			Product product = productRepository.findOne(itemCartR.getProduct());
			if (product == null || !product.isActive()) {
				throw new AppException(400,
						"Product " + itemCartR.getProduct() + " is not available or does not exist.");
			}

			OrderProduct item = new OrderProduct();
			item.setProduct(product);
			item.setQuantity(itemCart.getQuantity());
			item.setUnitPrice(product.getUnitPrice());
			item.setOrder(order);
			order.addItem(item);
		}
	}

	/**
	 * Create a new order for a give customer and set the order status to
	 * "In Progress"
	 * 
	 * @param customerId
	 * @return
	 * @throws AppException
	 */
	public OrderR create(OrderR orderR, Long customerId) throws AppException {
		Customer customer = customerRepository.findOne(customerId);
		Cart cart = customer.getCart();

		if (cart == null) {
			throw new IllegalArgumentException("The customer does not have a cart");
		}

		List<ItemCart> items = cart.getItems();

		if (items.isEmpty()) {
			throw new IllegalArgumentException("There is no item to add to the order");
		}

		Order order = new Order();
		Address shippingAddress = getAddressById(customer, orderR.getShippingAddress());

		order.setCustomer(customer);
		order.setShippingAddress(shippingAddress);
		// TODO define a good way to set order number
		// order.setNumber("1111");
		order.setOrderPlacedAt(LocalDateTime.now());
		// TODO calculate total price only on payment
		// order.setTotalPrice(getTotalPrice(items));
		order = orderRepositoy.save(order);

		addItemsToOrder(items, order);
		order.getStatus().add(new OrderStatus(Status.IN_PROGRESS));

		// remove the cart and save
		customer.setCart(null);
		customerRepository.save(customer);

		return new OrderR(order);
	}

	/**
	 * Get an address of a customer given the address id
	 * 
	 * @param customer
	 * @param addressId
	 * @return
	 * @throws AppException
	 */
	private Address getAddressById(Customer customer, Long addressId) throws AppException {
		List<Address> addresses = customer.getAddresses();
		for (Address address : addresses) {
			if (addressId == 0l) {
				return address;
			} else if (address.getId() == addressId) {
				return address;
			}
		}

		throw new AppException(404, "No address found for id " + addressId);
	}

	/**
	 * Add items to an order. Copy all items from a cart
	 * 
	 * @param listItemsCart
	 * @param order
	 * @return
	 */
	private void addItemsToOrder(List<ItemCart> listItemsCart, Order order) {
		for (ItemCart itemCart : listItemsCart) {
			OrderProduct item = new OrderProduct();
			item.setProduct(itemCart.getProduct());
			item.setQuantity(itemCart.getQuantity());
			item.setUnitPrice(itemCart.getProduct().getUnitPrice());
			item.setOrder(order);
			order.addItem(item);
		}
	}

	/**
	 * Get the total price of an order. It's the sum of items in a cart
	 * 
	 * @param items
	 * @return
	 */
	private BigDecimal getTotalPrice(List<OrderProduct> items) {
		BigDecimal result = BigDecimal.ZERO;

		for (OrderProduct item : items) {
			BigDecimal itemValue = item.getProduct().getUnitPrice().multiply(new BigDecimal(item.getQuantity()));
			result = result.add(itemValue);
		}

		return result;
	}

	/**
	 * Make the payment for an order. A payment means to associate a customer's
	 * credit card with the order and set the payment date as well. A payment
	 * can only be made if the order status is "In Progress"
	 * 
	 * @param id
	 * @return
	 * @throws AppException
	 */
	public OrderR makePayment(Long id) throws AppException {
		Order order = orderRepositoy.findOne(id);

		if (order == null) {
			throw new AppException(404,
					"A payment is not possible for this order. It has already been paid, shipped, delivered or cancelled");
		}

		OrderStatus status = getLastOrderStatus(order);

		if (status.getStatus().getId() >= Status.PAID.getId()) {
			throw new AppException(400, "This order has already been paid.");
		}

		// if (!status.getStatus().equals(Status.IN_PROGRESS)) {
		// throw new AppException(400, 400, "This order cannot be cancelled
		// anymore.", "", "");
		// }
		//
		// if (order.getCreditCard() != null) {
		// throw new AppException(400, 400, "This order has already been paid",
		// "", "");
		// }

		// check if products are available
		checkItemAvailability(order);

		// get the active credit card of a customer
		CreditCard creditCard = getCreditCard(order.getCustomer());

		order.setCreditCard(creditCard);
		order.setOrderPaidAt(LocalDateTime.now());
		order.setTotalPrice(getTotalPrice(order.getItems()));

		// Set the order status to paid
		order.getStatus().add(new OrderStatus(Status.PAID));

		updateItemsStatus(order.getItems());

		// TODO update the quantity of a Product (product.setQuantity)

		order = orderRepositoy.save(order);

		return new OrderR(order);
	}

	/**
	 * Set the status of each item to "2 = Fullfillment"
	 * 
	 * @param items
	 */
	private void updateItemsStatus(List<OrderProduct> items) {
		for (OrderProduct item : items) {
			item.setStatus("2"); // Ready for fulfillment
		}
	}

	/**
	 * Check if the product is available and if the there is enough quantity
	 * 
	 * @param order
	 * @return
	 * @throws AppException
	 */
	private boolean checkItemAvailability(Order order) throws AppException {

		for (OrderProduct item : order.getItems()) {
			Product product = item.getProduct();
			if (!product.isActive()) {
				throw new AppException(400, "The product " + product.getName() + " is no longer available");
			} else if (product.getQuantity() < item.getQuantity()) {
				throw new AppException(400,
						"Sorry, we don't have the quantity you want for the product " + product.getName());
			}
		}

		return true;
	}

	/**
	 * Return the current credit card of a customer
	 * 
	 * @param customer
	 * @return
	 * @throws AppException
	 */
	private CreditCard getCreditCard(Customer customer) throws AppException {
		List<CreditCard> listCards = customer.getCreditCards();
		CreditCard creditCard = null;

		for (CreditCard c : listCards) {
			if (c.isActive()) {
				creditCard = c;
				break;
			}
		}

		if (creditCard == null) {
			throw new AppException(400, "The customer does not have a credit card");
		}

		return creditCard;
	}

	private CreditCard getCreditCard(Customer customer, Long cCardId) throws AppException {
		List<CreditCard> listCards = customer.getCreditCards();
		CreditCard creditCard = null;

		for (CreditCard c : listCards) {
			if (c.isActive() && c.getId() == cCardId) {
				creditCard = c;
				break;
			}
		}

		if (creditCard == null) {
			throw new AppException(400, "The customer does not have a credit card");
		}

		return creditCard;
	}

	/**
	 * Cancel an order. An order can only be cancelled if it has not yet been
	 * shipped
	 * 
	 * @param id
	 * @return
	 * @throws AppException
	 */
	public OrderR cancel(Long id) throws AppException {
		Order order = orderRepositoy.findOne(id);

		if (order == null) {
			throw new AppException(404, "There is no order with this id");
		}

		OrderStatus status = getLastOrderStatus(order);

		if (status == null) {
			throw new AppException(500, "There is no status for this order");
		}

		if (status.getStatus().getId() >= Status.SHIPPED.getId()) {
			throw new AppException(400,
					"This order cannot be cancelled because it has already been shipped or delivered");
		}

		order.getStatus().add(new OrderStatus(Status.CANCELLED));

		return new OrderR(order);
	}

	/**
	 * Get the last status of an order
	 * 
	 * @param order
	 * @return
	 */
	private OrderStatus getLastOrderStatus(Order order) {
		List<OrderStatus> list = order.getStatus();

		if (!list.isEmpty()) {
			return list.get(list.size() - 1);
		}

		return null;
	}

	/**
	 * Get the current status of an order
	 * 
	 * @param orderId
	 * @return
	 * @throws AppException
	 */
	public OrderStatusR getOrderStatus(Long orderId) throws AppException {
		Order order = orderRepositoy.findOne(orderId);

		if (order == null) {
			throw new AppException(404, "There is no order with this id");
		}

		OrderStatus orderStatus = getLastOrderStatus(order);

		return new OrderStatusR(orderStatus);
	}

	public OrderR deleteItem(Long id, Long productId) {
		Order order = orderRepositoy.findOne(id);

		if (order == null) {
			throw new AppException(404, "No such order");
		}
		if (!isOrderInProgress(order)) {
			throw new AppException(400, "This order has already been paid, cancelled or shipped.");
		}

		for (OrderProduct item : order.getItems()) {
			if (item.getProduct().getId() == productId) {
				order.getItems().remove(item);
				break;
			}
		}

		return new OrderR(order);
	}

}
