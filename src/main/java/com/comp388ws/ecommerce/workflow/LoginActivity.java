package com.comp388ws.ecommerce.workflow;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.comp388ws.ecommerce.model.Customer;
import com.comp388ws.ecommerce.model.Login;
import com.comp388ws.ecommerce.model.Seller;
import com.comp388ws.ecommerce.repositories.LoginRepository;
import com.comp388ws.ecommerce.representation.CustomerR;
import com.comp388ws.ecommerce.representation.SellerR;

@Service
@Transactional
public class LoginActivity {

	private LoginRepository loginRepository;

	@Autowired
	public LoginActivity(LoginRepository userRepository) {
		this.loginRepository = userRepository;
	}

	public Login getById(Long id) {
		return null;
	}

	public Login getByUsername(String username) {
		return null;
	}
	
	public Login getByEmail(String email) {
		return loginRepository.findByEmail(email);
	}

	public Login getByCustomer(CustomerR customerR) {
		return loginRepository.findByCustomer(new Customer(customerR));
	}
	
	public Login getBySeller(SellerR sellerR) {
		return loginRepository.findBySeller(new Seller(sellerR));
	}

	public Login create(Login login) {
		if (login.getCustomer() != null) {
			login.setEmail(login.getCustomer().getEmail());
		} else {
			login.setEmail(login.getSeller().getEmail());
		}
		return loginRepository.save(login);
	}

	public Login update(Login login) {
		return null;
	}

	public void delete(Login login) {

	}
}
