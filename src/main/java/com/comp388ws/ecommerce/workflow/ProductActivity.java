package com.comp388ws.ecommerce.workflow;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.comp388ws.ecommerce.errors.AppException;
import com.comp388ws.ecommerce.model.Product;
import com.comp388ws.ecommerce.model.ProductDetail;
import com.comp388ws.ecommerce.repositories.ProductRepository;
import com.comp388ws.ecommerce.representation.ProductDetailR;
import com.comp388ws.ecommerce.representation.ProductR;

@Service("productService")
@Transactional
public class ProductActivity {

	private ProductRepository productRepository;

	@Autowired
	public ProductActivity(ProductRepository productRepository) {
		this.productRepository = productRepository;
	}

	public ProductR findById(Long id) throws AppException {
		Product product = productRepository.findOne(id);
		if (product == null) {
			throw new AppException(404, "There is no product with id " + id);
		}
		return new ProductR(product);
	}

	public List<ProductR> findAll() {
		Iterator<Product> products = productRepository.findAll().iterator();
		List<ProductR> list = new ArrayList<>();

		while (products.hasNext()) {
			list.add(new ProductR(products.next()));
		}

		return list;
	}

	public ProductR create(ProductR productR) {
		Product product = new Product(productR);
		product = productRepository.save(product);

		return new ProductR(product);
	}

	public ProductR update(ProductR productR) {
		Product p = productRepository.findOne(productR.getId());
		Product product = new Product(productR);

		product.setActive(p.isActive());
		product.setDetails(p.getDetails());
		product.setCreatedAt(p.getCreatedAt());

		product = productRepository.save(product);

		return new ProductR(product);
	}

	public void remove(Long id) throws AppException {
		Product product = productRepository.findOne(id);
		product.setActive(false);
		productRepository.save(product);
	}

	/**
	 * Add one detail to a given product
	 * 
	 * @param productId
	 * @param detail
	 * @return
	 * @throws AppException
	 */
	public ProductDetailR addProductDetail(Long productId, ProductDetailR detailR) throws AppException {
		Product product = productRepository.findOne(productId);
		ProductDetail detail = new ProductDetail(detailR);

		product.addDetail(detail);
		return new ProductDetailR(detail);
	}

	/**
	 * Returns all detail of a given product
	 * 
	 * @param productId
	 * @return
	 * @throws AppException
	 */
	public List<ProductDetailR> getProductDetails(Long productId) throws AppException {

		List<ProductDetail> details = productRepository.findOne(productId).getDetails();
		List<ProductDetailR> listR = new ArrayList<>();

		for (ProductDetail detail : details) {
			listR.add(new ProductDetailR(detail));
		}

		return listR;
	}

	/**
	 * Update a given detail
	 *
	 * @param detailId
	 * @return
	 * @throws AppException
	 */
	public ProductDetailR updateProductDetail(Long productId, ProductDetailR detailR) throws AppException {
		List<ProductDetail> details = productRepository.findOne(productId).getDetails();
		ProductDetail detail = getDetailById(detailR.getId(), details);
		detail.setKey(detailR.getKey());
		detail.setValue(detailR.getValue());

		return new ProductDetailR(detail);
	}

	private ProductDetail getDetailById(Long id, List<ProductDetail> details) throws AppException {
		for (ProductDetail d : details) {
			if (d.getId() == id) {
				return d;
			}
		}
		throw new AppException(404, "No detail found for id " + id);
	}

	private void removeDetailById(Long id, List<ProductDetail> details) throws AppException {
		Iterator<ProductDetail> it = details.iterator();
		while (it.hasNext()) {
			ProductDetail detail = it.next();
			if (detail.getId() == id) {
				it.remove();
				return;
			}
		}
		throw new AppException(404, "No detail found for id " + id);
	}

	/**
	 * Remove one detail
	 *
	 * @param id
	 * @throws AppException
	 */
	public void deleteProductDetail(Long productId, Long detailId) throws AppException {
		List<ProductDetail> details = productRepository.findOne(productId).getDetails();
		removeDetailById(detailId, details);
	}

	/**
	 * Remove all details of a given product
	 *
	 * @throws AppException
	 */
	public void deleteProductDetails(Long productId) throws AppException {
		List<ProductDetail> details = productRepository.findOne(productId).getDetails();
		details.clear();
	}

	public List<ProductR> findByNameOrDescription(String search) {
		List<Product> products = productRepository.findByNameOrDescriptionAndActiveIsTrue(search);
		List<ProductR> list = new ArrayList<>();

		for (Product product : products) {
			list.add(new ProductR(product));
		}

		return list;
	}

	public List<ProductR> findByCriteria(String search, String in) {
		List<Product> products = new ArrayList<>();
		List<ProductR> list = new ArrayList<>();

		if (search != null && !search.isEmpty()) {
			return findByNameOrDescription(search);
		} else if (in != null && !in.isEmpty()) {
			try {
				List<Long> ids = new ArrayList<>();
				in = in.replaceAll(",$", "");
				for (String id : in.split(",")) {
					ids.add(Long.valueOf(id));
				}
				if (ids.size() > 0) {
					products = productRepository.findByIdIn(ids);
				}
			} catch (Exception e) {
				throw e;
			}
		} else {
			return findAll();
		}

		for (Product product : products) {
			list.add(new ProductR(product));
		}

		return list;
	}
}
