package com.comp388ws.ecommerce.workflow;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.comp388ws.ecommerce.errors.AppException;
import com.comp388ws.ecommerce.model.Customer;
import com.comp388ws.ecommerce.model.Product;
import com.comp388ws.ecommerce.model.Review;
import com.comp388ws.ecommerce.model.Seller;
import com.comp388ws.ecommerce.repositories.CustomerRepository;
import com.comp388ws.ecommerce.repositories.ProductRepository;
import com.comp388ws.ecommerce.repositories.ReviewRepository;
import com.comp388ws.ecommerce.repositories.SellerRepository;
import com.comp388ws.ecommerce.representation.ProductReviewR;
import com.comp388ws.ecommerce.representation.SellerReviewR;

@Service("reviewActivity")
@Transactional
public class ReviewActivity {

	private ReviewRepository reviewRepository;
	private ProductRepository productRepository;
	private SellerRepository sellerRepository;
	private CustomerRepository customerRepository;

	@Autowired
	public ReviewActivity(ReviewRepository reviewRepository, ProductRepository productRepository,
			SellerRepository sellerRepository, CustomerRepository customerRepository) {
		this.reviewRepository = reviewRepository;
		this.productRepository = productRepository;
		this.sellerRepository = sellerRepository;
		this.customerRepository = customerRepository;
	}

	/**
	 * Returns one review of a product
	 * 
	 * @param reviewId
	 * @return
	 */
	public ProductReviewR getProductReview(Long reviewId) {
		Review review = reviewRepository.findOne(reviewId);
		Assert.notNull(review, "No review found for this id");

		return new ProductReviewR(review);
	}

	/**
	 * Returns all reviews of a product
	 * 
	 * @param productId
	 * @return
	 */
	public List<ProductReviewR> getProductReviews(Long productId) {
		Product product = productRepository.findOne(productId);
		List<Review> reviews = reviewRepository.findByProduct(product);
		List<ProductReviewR> list = new ArrayList<>();

		for (Review review : reviews) {
			list.add(new ProductReviewR(review));
		}

		return list;
	}

	/**
	 * Adds a review to a product. A customer can only add a review for each
	 * product
	 * 
	 * @param productId
	 * @param reviewR
	 * @return
	 * @throws AppException
	 */
	public ProductReviewR addProductReview(Long productId, ProductReviewR reviewR) throws AppException {
		Product product = productRepository.findOne(productId);
		Customer customer = new Customer();
		customer.setId(reviewR.getCustomer());

		Review review = reviewRepository.findByProductAndCustomer(product, customer);
		if (review != null) {
			throw new AppException(400, "The customer has already added a review for this product");
		}

		review = new Review(reviewR, product, customer);
		review = saveReview(review);

		return new ProductReviewR(review);
	}

	private Review saveReview(Review review) {
		return reviewRepository.save(review);
	}

	/**
	 * Return one seller review given the review's id
	 * 
	 * @param reviewId
	 * @return
	 */
	public SellerReviewR getSellerReview(Long reviewId) {
		Review review = reviewRepository.findOne(reviewId);
		Assert.notNull(review, "No review found for this id");

		return new SellerReviewR(review);
	}

	/**
	 * Returns all reviews for a seller given the seller's id
	 * 
	 * @param sellerId
	 * @return
	 */
	public List<SellerReviewR> getSellerReviews(Long sellerId) {
		Seller seller = sellerRepository.findOne(sellerId);
		List<Review> reviews = reviewRepository.findBySeller(seller);
		List<SellerReviewR> list = new ArrayList<>();

		for (Review review : reviews) {
			list.add(new SellerReviewR(review));
		}

		return list;
	}

	/**
	 * Adds a review for a seller
	 * 
	 * @param sellerId
	 * @param reviewR
	 * @return
	 * @throws AppException
	 */
	public SellerReviewR addSellerReview(Long sellerId, SellerReviewR reviewR) throws AppException {
		Seller seller = sellerRepository.findOne(sellerId);
		Customer customer = customerRepository.findOne(reviewR.getCustomerId());

		Review review = reviewRepository.findBySellerAndCustomer(seller, customer);
		if (review != null) {
			throw new AppException(400, "The customer has already added a review for this seller");
		}

		review = new Review(reviewR, seller, customer);
		review = saveReview(review);

		return new SellerReviewR(review);
	}
}
