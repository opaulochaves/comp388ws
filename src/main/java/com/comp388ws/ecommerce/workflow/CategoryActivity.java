package com.comp388ws.ecommerce.workflow;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.comp388ws.ecommerce.errors.AppException;
import com.comp388ws.ecommerce.model.Category;
import com.comp388ws.ecommerce.repositories.CategoryRepository;
import com.comp388ws.ecommerce.representation.CategoryR;

@Service("categoryService")
@Transactional
public class CategoryActivity {

	private CategoryRepository categoryRepository;

	@Autowired
	public CategoryActivity(CategoryRepository categoryRepository) {
		this.categoryRepository = categoryRepository;
	}

	public CategoryR getById(Long id) throws AppException {
		Category acategory = categoryRepository.findOne(id);
		if (acategory == null) {
			throw new AppException(404, "There is no category with id " + id);
		}
		return new CategoryR(acategory);
	}

	public CategoryR create(CategoryR categoryR) {
		Category aCategory = new Category(categoryR);
		aCategory = categoryRepository.save(aCategory);

		return new CategoryR(aCategory);
	}

	/**
	 * Change later to join create and update methods
	 * 
	 * @param categoryR
	 * @return
	 */
	public CategoryR update(CategoryR categoryR) {
		Category aCategory = new Category(categoryR);
		aCategory = categoryRepository.save(aCategory);

		return new CategoryR(aCategory);
	}

	public void remove(Long id) {
		categoryRepository.delete(id);
	}

	public List<CategoryR> getAll() {
		Iterator<Category> categories = categoryRepository.findAll().iterator();
		List<CategoryR> list = new ArrayList<>();

		while (categories.hasNext()) {
			list.add(new CategoryR(categories.next()));
		}

		return list;
	}

}
