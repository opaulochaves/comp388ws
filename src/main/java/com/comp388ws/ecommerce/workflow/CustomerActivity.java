package com.comp388ws.ecommerce.workflow;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.comp388ws.ecommerce.errors.AppException;
import com.comp388ws.ecommerce.model.Address;
import com.comp388ws.ecommerce.model.CreditCard;
import com.comp388ws.ecommerce.model.Customer;
import com.comp388ws.ecommerce.model.Login;
import com.comp388ws.ecommerce.repositories.CreditCardRepository;
import com.comp388ws.ecommerce.repositories.CustomerRepository;
import com.comp388ws.ecommerce.repositories.LoginRepository;
import com.comp388ws.ecommerce.representation.AddressR;
import com.comp388ws.ecommerce.representation.CreditCardR;
import com.comp388ws.ecommerce.representation.CustomerR;

@Service("customerService")
@Transactional
public class CustomerActivity {

	private CustomerRepository customerRepository;
	private CreditCardRepository creditCardRepository;
	private LoginRepository loginRepository;

	@Autowired
	public CustomerActivity(CustomerRepository customerRepository, CreditCardRepository creditCardRepository,
			LoginRepository loginRepository) {
		this.customerRepository = customerRepository;
		this.creditCardRepository = creditCardRepository;
		this.loginRepository = loginRepository;
	}

	/**
	 * Returns a customer given his id
	 * 
	 * @param id
	 * @return
	 * @throws AppException
	 */
	private Customer getCustomer(Long id) {
		Customer customer = customerRepository.findOne(id);
		if (customer == null) {
			throw new AppException(404, "No customer found with id " + id);
		}
		return customer;
	}

	public CustomerR getById(Long id) throws AppException {
		Customer aCustomer = getCustomer(id);
		Login login = loginRepository.findByCustomer(aCustomer);
		CustomerR customerR = new CustomerR(aCustomer, login);

		return customerR;
	}

	/**
	 * Return a list of Customer Representation
	 */
	public List<CustomerR> getAll() {
		Iterator<Customer> customers = customerRepository.findAll().iterator();
		List<CustomerR> list = new ArrayList<>();

		while (customers.hasNext()) {
			list.add(new CustomerR(customers.next()));
		}

		return list;
	}

	/**
	 * Adds a new customer
	 * 
	 * @param customerR
	 * @return
	 */
	public CustomerR create(CustomerR customerR) {
		Customer customer = new Customer(customerR);

		customer = customerRepository.save(customer);

		Login login = new Login(customer, customerR.getUsername(), customerR.getPassword());

		login = loginRepository.save(login);

		return new CustomerR(customer, login);
	}

	/**
	 * Updates a customer
	 * 
	 * @param customerR
	 * @return
	 * @throws AppException
	 */
	public CustomerR update(CustomerR customerR) throws AppException {
		Customer aCustomer = getCustomer(customerR.getId());
		aCustomer.setFirstName(customerR.getFirstName());
		aCustomer.setLastName(customerR.getLastName());
		aCustomer.setPhone(customerR.getPhone());
		aCustomer = customerRepository.save(aCustomer);

		Login login = loginRepository.findByCustomer(aCustomer);

		String username = customerR.getUsername();
		if (username != null && !username.isEmpty()) {
			login.setUsername(username);
		}

		String password = customerR.getPassword();
		if (password != null && !password.isEmpty()) {
			login.setPassword(password);
		}

		login = loginRepository.save(login);

		return new CustomerR(aCustomer, login);
	}

	public void remove(Long id) {
	}

	/**
	 * Add a credit card to a customer. A customer can have only one credit card
	 * associated with his account
	 * 
	 * @param customer
	 * @param creditCard
	 * @return
	 * @throws AppException
	 */
	public CreditCardR addCreditCard(CustomerR customerR, CreditCardR creditCardR) throws AppException {
		Customer aCustomer = getCustomer(customerR.getId());
		CreditCard creditCard = new CreditCard(creditCardR);

		CreditCard cCard = creditCardRepository.findByCustomerAndActiveIsTrue(aCustomer);
		if (cCard != null) {
			throw new AppException(400, "Customer with ID " + aCustomer.getId() + " already has a credit card ");
		}

		creditCard.setCustomer(aCustomer);
		creditCard = creditCardRepository.save(creditCard);
		aCustomer.addCreditCard(creditCard);
		customerRepository.save(aCustomer);

		creditCardR.setId(creditCard.getId());
		return creditCardR;
	}

	/**
	 * Return the credit card information of a customer
	 * 
	 * @param customer
	 * @return
	 * @throws AppException
	 */
	public CreditCardR getCreditCard(CustomerR customerR) throws AppException {
		Customer customer = new Customer(customerR);

		CreditCard cCard = creditCardRepository.findByCustomerAndActiveIsTrue(customer);
		if (cCard == null) {
			throw new AppException(404, "Customer with ID " + customer.getId() + " does not have a credit card ");
		}

		return new CreditCardR(cCard);
	}

	/**
	 * Update a credit card of a given customer
	 * 
	 * @param customer
	 * @param creditCard
	 * @return
	 * @throws AppException
	 */
	public CreditCardR updateCreditCard(CustomerR customerR, CreditCardR creditCardR) throws AppException {
		Customer customer = new Customer(customerR);

		CreditCard cCard = creditCardRepository.findByCustomerAndActiveIsTrue(customer);

		if (cCard == null) {
			throw new AppException(400,
					"Customer with ID " + customer.getId() + " does not have a credit card to update.");
		}

		creditCardR.setId(cCard.getId());
		CreditCard creditCard = new CreditCard(creditCardR);
		creditCard.setCustomer(customer);
		creditCard = creditCardRepository.save(creditCard);

		return new CreditCardR(creditCard);
	}

	public void deleteCreditCard(CustomerR customerR) throws AppException {
		Customer customer = new Customer(customerR);

		CreditCard creditCard = creditCardRepository.findByCustomerAndActiveIsTrue(customer);
		if (creditCard == null) {
			throw new AppException(400,
					"Customer with ID " + customer.getId() + " does not have a credit card to update.");
		}
		creditCard.setActive(false);
		creditCardRepository.save(creditCard);
	}

	/**
	 * Return the address of a given customer
	 * 
	 * @param customer
	 * @return
	 * @throws AppException
	 */
	public AddressR getAddress(Long customerId, int id) throws AppException {
		Customer aCustomer = getCustomer(customerId);

		return new AddressR(getAddressById(aCustomer, id));
	}

	/**
	 * Get an address of a customer given the address id
	 * 
	 * @param customer
	 * @param addressId
	 * @return
	 * @throws AppException
	 */
	private Address getAddressById(Customer customer, int addressId) throws AppException {
		List<Address> addresses = customer.getAddresses();
		for (Address address : addresses) {
			if (address.getId() == addressId) {
				return address;
			}
		}

		throw new AppException(404, "No address found for id " + addressId);
	}

	private void removeAddressById(Customer customer, int addressId) throws AppException {
		List<Address> addresses = customer.getAddresses();
		for (Address address : addresses) {
			if (address.getId() == addressId) {
				addresses.remove(address);
				return;
			}
		}

		throw new AppException(404, "No address found for id " + addressId);
	}

	public List<AddressR> getAddresses(Long customerId) throws AppException {
		Customer aCustomer = getCustomer(customerId);
		List<AddressR> list = new ArrayList<>();

		for (Address address : aCustomer.getAddresses()) {
			list.add(new AddressR(address));
		}

		return list;
	}

	/**
	 * Add an address to a customer. A customer can only have one address
	 * 
	 * @param customerId
	 * @param addressR
	 * @return
	 * @throws AppException
	 */
	public AddressR addAddress(Long customerId, AddressR addressR) throws AppException {
		Customer aCustomer = getCustomer(customerId);

		Address address = new Address(addressR);
		aCustomer.getAddresses().add(address);

		aCustomer = customerRepository.save(aCustomer);

		return new AddressR(address);
	}

	/**
	 * Updates an address of a customer
	 * 
	 * @param customerId
	 * @param id
	 * @param addressR
	 * @return
	 * @throws AppException
	 */
	public AddressR updateAddress(Long customerId, int id, AddressR addressR) throws AppException {
		Customer aCustomer = getCustomer(customerId);
		Address address = null;

		try {
			address = getAddressById(aCustomer, id);

			address.setLine1(addressR.getLine1());
			address.setLine2(addressR.getLine2());
			address.setCity(addressR.getCity());
			address.setState(addressR.getState());
			address.setType(addressR.getType());
			address.setZipcode(addressR.getZipcode());
			address.setCountry(addressR.getCountry());

			aCustomer = customerRepository.save(aCustomer);

		} catch (Exception e) {
			throw new AppException(404, "No address found for id " + id);
		}

		return new AddressR(address);
	}

	/**
	 * Delete the address of a customer
	 * 
	 * @param customerId
	 * @throws AppException
	 */
	public void deleteAddress(Long customerId, int id) throws AppException {
		Customer customer = customerRepository.findOne(customerId);
		removeAddressById(customer, id);
		customerRepository.save(customer);
	}
}
