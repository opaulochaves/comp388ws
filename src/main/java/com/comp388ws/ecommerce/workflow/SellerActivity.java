package com.comp388ws.ecommerce.workflow;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.comp388ws.ecommerce.errors.AppException;
import com.comp388ws.ecommerce.model.Login;
import com.comp388ws.ecommerce.model.Order;
import com.comp388ws.ecommerce.model.OrderProduct;
import com.comp388ws.ecommerce.model.OrderStatus;
import com.comp388ws.ecommerce.model.Product;
import com.comp388ws.ecommerce.model.Seller;
import com.comp388ws.ecommerce.model.Status;
import com.comp388ws.ecommerce.repositories.LoginRepository;
import com.comp388ws.ecommerce.repositories.OrderRepository;
import com.comp388ws.ecommerce.repositories.ProductRepository;
import com.comp388ws.ecommerce.repositories.SellerRepository;
import com.comp388ws.ecommerce.representation.OrderR;
import com.comp388ws.ecommerce.representation.ProductR;
import com.comp388ws.ecommerce.representation.SellerR;

@Service("sellerService")
@Transactional
public class SellerActivity {

	private SellerRepository sellerRepository;
	private ProductRepository productRepository;
	private LoginRepository loginRepository;

	@Autowired
	private OrderRepository orderRepository;

	@Autowired
	public SellerActivity(SellerRepository sellerRepository, ProductRepository productRepository,
			LoginRepository loginRepository) {
		this.sellerRepository = sellerRepository;
		this.productRepository = productRepository;
		this.loginRepository = loginRepository;
	}

	private Seller getSeller(Long id) throws AppException {
		Seller aSeller = sellerRepository.findOne(id);
		if (aSeller == null) {
			throw new AppException(404, "No seller found with id " + id);
		}
		return aSeller;
	}

	public SellerR getById(Long id) throws AppException {
		Seller aSeller = getSeller(id);
		SellerR sellerR = new SellerR(aSeller, loginRepository.findBySeller(aSeller));
		return sellerR;
	}

	/**
	 * Adds a new seller
	 * 
	 * @param sellerR
	 * @return
	 */
	public SellerR create(SellerR sellerR) {
		Seller aSeller = new Seller(sellerR);
		aSeller = sellerRepository.save(aSeller);

		Login login = new Login(aSeller, sellerR.getUsername(), sellerR.getPassword());
		login = loginRepository.save(login);

		return new SellerR(aSeller, login);
	}

	/**
	 * Updates a seller
	 * 
	 * @param sellerR
	 * @return
	 * @throws AppException
	 */
	public SellerR udpate(SellerR sellerR) throws AppException {
		Seller aSeller = getSeller(sellerR.getId());
		aSeller.setFirstName(sellerR.getFirstName());
		aSeller.setLastName(sellerR.getLastName());
		aSeller.setPhone(sellerR.getPhone());
		aSeller.setCompanyName(sellerR.getCompanyName());
		aSeller = sellerRepository.save(aSeller);

		Login login = loginRepository.findBySeller(aSeller);

		String username = sellerR.getUsername();
		if (username != null && !username.isEmpty()) {
			login.setUsername(username);
		}

		String password = sellerR.getPassword();
		if (password != null && !password.isEmpty()) {
			login.setPassword(password);
		}

		login = loginRepository.save(login);
		return new SellerR(aSeller, login);
	}

	/**
	 * Remove a given seller, setting its active field to false
	 * 
	 * @param id
	 * @throws AppException
	 */
	public void remove(Long id) throws AppException {
		Seller aSeller = getSeller(id);
		aSeller.setActive(false);
		sellerRepository.save(aSeller);
	}

	/**
	 * Returns a list of all seller representations
	 * 
	 * @return
	 */
	public List<SellerR> getAll() {
		Iterator<Seller> sellers = sellerRepository.findAll().iterator();
		List<SellerR> list = new ArrayList<>();

		while (sellers.hasNext()) {
			list.add(new SellerR(sellers.next()));
		}

		return list;
	}

	public List<ProductR> getAllProducts(Long id) throws AppException {
		Seller aSeller = getSeller(id);
		List<Product> products = productRepository.findBySeller(aSeller);
		List<ProductR> list = new ArrayList<>();

		for (Product p : products) {
			list.add(new ProductR(p));
		}

		return list;
	}

	public List<OrderR> getOrdersForFulfillment(Long id) throws AppException {
		Seller aSeller = getSeller(id);
		List<Order> orders = orderRepository.findOrderForFulfillment(aSeller);
		List<OrderR> list = new ArrayList<>();

		for (Order o : orders) {
			list.add(new OrderR(o));
		}

		return list;
	}

	/**
	 * Fulfill an item of an order by adding a tracking number and changing its
	 * state to Shipped. If all items have the same tracking number, then add
	 * the tracking number to order as well. After shipping all items change
	 * order status to Shipped also
	 * 
	 * @param sellerId
	 * @param orderId
	 * @param itemId
	 * @param trackingNumber
	 * @throws AppException
	 */
	public OrderR fulfillOrder(Long sellerId, Long orderId, Long itemId, String trackingNumber) throws AppException {
		Assert.notNull(trackingNumber, "Tracking number can't be empty");
		Seller aSeller = getSeller(sellerId);

		Order orderFulfillment = orderRepository.findOrderForFulfillment(aSeller, itemId);

		if (orderFulfillment == null) {
			throw new AppException(404, "No order found for fulfillment.");
		}

		OrderProduct item = orderFulfillment.getItems().get(0);
		item.setTrackingNumber(trackingNumber);
		item.setStatus("3"); // TODO Shipped (create an Enum)

		orderFulfillment = orderRepository.save(orderFulfillment);

		Order order = orderRepository.findOne(orderId);
		boolean allItemsShipped = true;

		for (OrderProduct i : order.getItems()) {
			// if there's an item not shipped yet
			if (!i.getStatus().equals("3")) {
				allItemsShipped = false;
				break;
			} else {
				if (trackingNumber != null && !i.getTrackingNumber().equals(trackingNumber)) {
					trackingNumber = null;
				}
			}
		}

		// if all items has been shipped, update the order
		if (allItemsShipped) {
			order.getStatus().add(new OrderStatus(Status.SHIPPED));
			if (trackingNumber != null) {
				order.setTrackingNumber(trackingNumber);
			}
			orderRepository.save(order);
		}

		return new OrderR(orderFulfillment);
	}

}
