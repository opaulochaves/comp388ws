package com.comp388ws.ecommerce.workflow;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.comp388ws.ecommerce.errors.AppException;
import com.comp388ws.ecommerce.model.Cart;
import com.comp388ws.ecommerce.model.Customer;
import com.comp388ws.ecommerce.model.ItemCart;
import com.comp388ws.ecommerce.repositories.CustomerRepository;
import com.comp388ws.ecommerce.representation.CartR;
import com.comp388ws.ecommerce.representation.ItemCartR;

@Service("cartActivity")
@Transactional
public class CartActivity {

	private CustomerRepository customerRepository;

	@Autowired
	public CartActivity(CustomerRepository customerRepository) {
		this.customerRepository = customerRepository;
	}

	public CartR findByCustomer(Long customerId) {
		Customer customer = customerRepository.findOne(customerId);
		Cart cart = customer.getCart();

		if (cart == null) {
			return null;
		}
		return new CartR(cart);
	}

	public CartR create(Long customerId) {
		Customer customer = customerRepository.findOne(customerId);

		Cart cart = new Cart();
		customer.setCart(cart);

		customer = customerRepository.save(customer);

		return new CartR(customer.getCart());
	}

	public void remove(Long customerId) {
		Customer customer = customerRepository.findOne(customerId);
		customer.setCart(null);

		customerRepository.save(customer);
	}

	/**
	 * Add a new item to a cart of a given customer
	 * 
	 * @param customerId
	 * @param itemR
	 * @return
	 */
	public CartR addItem(Long customerId, ItemCartR itemR) {
		Customer customer = customerRepository.findOne(customerId);
		Cart cart = customer.getCart();

		if (cart == null) {
			cart = new Cart();
			customer.setCart(cart);
		}

		ItemCart item = new ItemCart(itemR);
		item.setCart(cart);
		cart.getItems().add(item);

		// with this, inserts twice
		// customer = customerRepository.save(customer);

		return new CartR(customer.getCart());
	}

	/**
	 * Remove an item from the cart
	 * 
	 * @param customerId
	 * @param id
	 * @return
	 * @throws AppException
	 */
	public CartR removeItem(Long customerId, int id) throws AppException {
		Customer customer = customerRepository.findOne(customerId);
		Cart cart = customer.getCart();

		if (cart == null) {
			throw new AppException(404, "No cart found");
		}

		try {
			cart.getItems().remove(id - 1);
			customer.setCart(cart);

			customer = customerRepository.save(customer);
		} catch (Exception e) {
			System.out.println(e);
			throw new AppException(404, "The item could not be deleted.");
		}

		return new CartR(customer.getCart());
	}

	public CartR saveCart(Long customerId, CartR cartR) {
		Customer customer = customerRepository.findOne(customerId);
		Cart cart = customer.getCart();

		if (cart == null) {
			customer.setCart(new Cart());
		} else {
			cart.getItems().clear();
			customer.setCart(cart);
		}
		customer = customerRepository.save(customer);
		cart = customer.getCart();

		for (ItemCartR itemR : cartR.getItems()) {
			ItemCart itemCart = new ItemCart(itemR);
			itemCart.setCart(cart);
			cart.getItems().add(itemCart);
		}

		return new CartR(customer.getCart());
	}

	/**
	 * Update the quantity of an item in the cart
	 * 
	 * @param customerId
	 * @param id
	 * @param item
	 * @return
	 * @throws AppException
	 */
	public CartR updateItem(Long customerId, int id, ItemCartR item) throws AppException {
		Customer customer = customerRepository.findOne(customerId);
		Cart cart = customer.getCart();
		ItemCart itemCart = null;

		if (cart == null) {
			throw new AppException(404, "No cart found");
		}

		if (item.getQuantity() < 1) {
			throw new AppException(404, "Quantity must be greater than 0");
		}

		try {
			itemCart = cart.getItems().get(id - 1);
			itemCart.setQuantity(item.getQuantity());

			customerRepository.save(customer);
		} catch (Exception e) {
			System.out.println(e);
			throw new AppException(404, "The item could not be updated.");
		}

		return new CartR(cart);
	}

}
