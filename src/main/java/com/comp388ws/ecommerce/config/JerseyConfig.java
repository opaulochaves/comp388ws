package com.comp388ws.ecommerce.config;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.ServerProperties;
import org.springframework.stereotype.Component;

import com.comp388ws.ecommerce.EcommerceApplication;

@Component
@ApplicationPath(value = "/api")
public class JerseyConfig extends ResourceConfig {

	public JerseyConfig() {
		packages(EcommerceApplication.class.getPackage().getName());

		property(ServerProperties.BV_SEND_ERROR_IN_RESPONSE, true);
		property(ServerProperties.BV_DISABLE_VALIDATE_ON_EXECUTABLE_OVERRIDE_CHECK, true);
	}

}
