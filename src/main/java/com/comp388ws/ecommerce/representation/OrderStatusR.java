package com.comp388ws.ecommerce.representation;

import java.time.LocalDateTime;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.comp388ws.ecommerce.model.OrderStatus;
import com.comp388ws.ecommerce.model.Status;
import com.comp388ws.ecommerce.util.LocalDateTimeXmlAdapter;

@XmlRootElement(name = "OrderStatus")
@XmlAccessorType(XmlAccessType.FIELD)
public class OrderStatusR {

	private String status;

	@XmlElement(name = "updated_at")
	@XmlJavaTypeAdapter(value = LocalDateTimeXmlAdapter.class)
	private LocalDateTime updatedAt;

	public OrderStatusR() {
	}

	public OrderStatusR(OrderStatus orderStatus) {
		this.updatedAt = orderStatus.getUpdatedAt();
		this.setStatus(getStatusDescription(orderStatus.getStatus()));
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public LocalDateTime getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(LocalDateTime updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getStatusDescription(Status status) {
		if (status.equals(Status.IN_PROGRESS)) {
			return "In Progress";
		} else if (status.equals(Status.PAID)) {
			return "Paid";
		} else if (status.equals(Status.CANCELLED)) {
			return "Cancelled";
		} else if (status.equals(Status.SHIPPED)) {
			return "Shipped";
		} else if (status.equals(Status.DELIVERED)) {
			return "Delivered";
		}

		return "";
	}
}
