package com.comp388ws.ecommerce.representation;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.comp388ws.ecommerce.model.Order;
import com.comp388ws.ecommerce.model.OrderProduct;
import com.comp388ws.ecommerce.model.OrderStatus;
import com.comp388ws.ecommerce.util.LocalDateTimeXmlAdapter;

@XmlRootElement(name = "Order")
@XmlAccessorType(XmlAccessType.FIELD)
public class OrderR extends BaseR {

	private Long id;

	private String number;

	@XmlElement(name = "tracking_number")
	private String trackingNumber;

	private Long customer;

	@XmlElement(name = "placed_at")
	@XmlJavaTypeAdapter(value = LocalDateTimeXmlAdapter.class)
	private LocalDateTime orderPlacedAt;

	@XmlElement(name = "total_price")
	private BigDecimal totalPrice;

	@XmlElement(name = "paid_at")
	@XmlJavaTypeAdapter(value = LocalDateTimeXmlAdapter.class)
	private LocalDateTime orderPaidAt;

	private List<OrderStatusR> status = new ArrayList<>();

	private List<OrderProductR> items = new ArrayList<>();

	// TODO add uri to it on the response (/customers/{cId}/addresses/{aId})
	@XmlElement(name = "shipping_address")
	private Long shippingAddress;

	public OrderR() {
	}

	public OrderR(Order order) {
		this.setId(order.getId());
		this.setNumber(order.getNumber());
		this.setTrackingNumber(order.getTrackingNumber());
		this.setCustomer(order.getCustomer().getId());
		this.setOrderPlacedAt(order.getOrderPlacedAt());
		this.setTotalPrice(order.getTotalPrice());
		this.setOrderPaidAt(order.getOrderPaidAt());

		if (order.getShippingAddress() != null) {
			this.setShippingAddress(order.getShippingAddress().getId());
		}

		List<OrderProduct> oItems = order.getItems();
		for (OrderProduct item : oItems) {
			items.add(new OrderProductR(item));
		}

		for (OrderStatus s : order.getStatus()) {
			status.add(new OrderStatusR(s));
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getTrackingNumber() {
		return trackingNumber;
	}

	public void setTrackingNumber(String trackingNumber) {
		this.trackingNumber = trackingNumber;
	}

	public Long getCustomer() {
		return customer;
	}

	public void setCustomer(Long customer) {
		this.customer = customer;
	}

	public LocalDateTime getOrderPlacedAt() {
		return orderPlacedAt;
	}

	public void setOrderPlacedAt(LocalDateTime orderPlacedAt) {
		this.orderPlacedAt = orderPlacedAt;
	}

	public BigDecimal getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(BigDecimal totalPrice) {
		this.totalPrice = totalPrice;
	}

	public LocalDateTime getOrderPaidAt() {
		return orderPaidAt;
	}

	public void setOrderPaidAt(LocalDateTime orderPaidAt) {
		this.orderPaidAt = orderPaidAt;
	}

	public List<OrderProductR> getItems() {
		return items;
	}

	public void setItems(List<OrderProductR> items) {
		this.items = items;
	}

	public List<OrderStatusR> getStatus() {
		return status;
	}

	public void setStatus(List<OrderStatusR> status) {
		this.status = status;
	}

	public Long getShippingAddress() {
		return shippingAddress;
	}

	public void setShippingAddress(Long shippingAddress) {
		this.shippingAddress = shippingAddress;
	}

}
