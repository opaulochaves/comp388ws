package com.comp388ws.ecommerce.representation;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.comp388ws.ecommerce.model.OrderProduct;

@XmlRootElement(name = "OrderProduct")
@XmlAccessorType(XmlAccessType.FIELD)
public class OrderProductR {

	private Long id;

	private int quantity;

	@XmlElement(name = "unit_price")
	private BigDecimal unitPrice;

	private Long product;
	
	@XmlElement(name = "product_name")
	private String productName;

	@XmlElement(name = "tracking_number")
	private String trackingNumber;

	// description of status
	private String status;

	public OrderProductR() {
	}

	public OrderProductR(OrderProduct item) {
		this.setId(item.getId());
		this.setQuantity(item.getQuantity());
		this.setUnitPrice(item.getProduct().getUnitPrice());
		this.setProduct(item.getProduct().getId());
		this.setProductName(item.getProduct().getName());
		this.setTrackingNumber(item.getTrackingNumber());
		this.setStatus(getStatusDescription(item.getStatus()));
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}

	public Long getProduct() {
		return product;
	}

	public void setProduct(Long product) {
		this.product = product;
	}

	public String getTrackingNumber() {
		return trackingNumber;
	}

	public void setTrackingNumber(String trackingNumber) {
		this.trackingNumber = trackingNumber;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	private String getStatusDescription(String status) {
		String res = null;
		if (status != null) {
			if (status.equals("1")) {
				res = "Cancelled";
			} else if (status.equals("2")) {
				res = "Fulfillment";
			} else if (status.equals("3")) {
				res = "Shipped";
			} else {
				res = "Delivered";
			}
		}
		return res;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}
}
