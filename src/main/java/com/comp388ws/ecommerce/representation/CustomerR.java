package com.comp388ws.ecommerce.representation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.comp388ws.ecommerce.model.Customer;
import com.comp388ws.ecommerce.model.Login;

@XmlRootElement(name = "Customer")
@XmlAccessorType(XmlAccessType.FIELD)
public class CustomerR extends BaseR {

	private Long id;

	@XmlElement(name = "first_name")
	private String firstName;

	@XmlElement(name = "last_name")
	private String lastName;

	private String email;

	private String phone;

	private String username;

	private String password;
	
	private Boolean active;

	public CustomerR() {
	}

	/**
	 * Create a Customer representation with login info
	 * 
	 * @param customer
	 * @param login
	 */
	public CustomerR(Customer customer, Login login) {
		this.setId(customer.getId());
		this.setEmail(customer.getEmail());
		this.setFirstName(customer.getFirstName());
		this.setLastName(customer.getLastName());
		this.setPhone(customer.getPhone());
		this.setActive(customer.getActive());
		this.setUsername(login.getUsername());
		this.setPassword(login.getPassword()); // remove password from response
	}

	/**
	 * Create a Customer representation without login info
	 * 
	 * @param customer
	 */
	public CustomerR(Customer customer) {
		this.setId(customer.getId());
		this.setEmail(customer.getEmail());
		this.setFirstName(customer.getFirstName());
		this.setLastName(customer.getLastName());
		this.setPhone(customer.getPhone());
		this.setActive(customer.getActive());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

}
