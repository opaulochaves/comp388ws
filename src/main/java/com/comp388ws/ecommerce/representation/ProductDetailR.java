package com.comp388ws.ecommerce.representation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.comp388ws.ecommerce.model.ProductDetail;

/**
 * 
 * @author Onontsatsal
 *
 */
@XmlRootElement(name = "ProductDetail")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
public class ProductDetailR {

	private Long id;

	private String key;

	private String value;

	public ProductDetailR() {
	}

	public ProductDetailR(ProductDetail productDetail) {
		this.setId(productDetail.getId());
		this.setKey(productDetail.getKey());
		this.setValue(productDetail.getValue());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
