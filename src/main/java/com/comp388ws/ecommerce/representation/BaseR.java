package com.comp388ws.ecommerce.representation;

import java.util.ArrayList;
import java.util.List;

public class BaseR {

	private List<Link> links = new ArrayList<>();

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	public void addLink(String href, String rel, String action) {
		Link link = new Link(href, rel, action);
		getLinks().add(link);
	}

}
