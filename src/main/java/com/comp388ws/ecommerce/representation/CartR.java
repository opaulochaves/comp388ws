package com.comp388ws.ecommerce.representation;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.comp388ws.ecommerce.model.Cart;
import com.comp388ws.ecommerce.model.ItemCart;
import com.comp388ws.ecommerce.util.LocalDateTimeXmlAdapter;

@XmlRootElement(name = "Cart")
@XmlAccessorType(XmlAccessType.FIELD)
public class CartR extends BaseR {

	@XmlElement(name = "created_at")
	@XmlJavaTypeAdapter(value = LocalDateTimeXmlAdapter.class)
	private LocalDateTime createdAd;

	private List<ItemCartR> items = new ArrayList<>();

	public CartR() {
	}

	public CartR(Cart cart) {
		this.setCreatedAd(cart.getCreatedAd());

		List<ItemCart> list = cart.getItems();

		if (list != null) {
			for (ItemCart item : list) {
				items.add(new ItemCartR(item));
			}
		}
	}

	public LocalDateTime getCreatedAd() {
		return createdAd;
	}

	public void setCreatedAd(LocalDateTime createdAd) {
		this.createdAd = createdAd;
	}

	public List<ItemCartR> getItems() {
		return items;
	}

	public void setItems(List<ItemCartR> items) {
		this.items = items;
	}

}
