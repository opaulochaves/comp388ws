package com.comp388ws.ecommerce.representation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.comp388ws.ecommerce.model.ItemCart;

@XmlRootElement(name = "ItemCart")
@XmlAccessorType(XmlAccessType.FIELD)
public class ItemCartR {

	private Long product;

	private int quantity;

	public ItemCartR() {
	}

	public ItemCartR(ItemCart itemCart) {
		this.setProduct(itemCart.getProduct().getId());
		this.setQuantity(itemCart.getQuantity());
	}

	public Long getProduct() {
		return product;
	}

	public void setProduct(Long product) {
		this.product = product;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

}
