package com.comp388ws.ecommerce.representation;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class NewOrderR {

	private List<ItemCartR> items = new ArrayList<>();

	private Long customer;

	@XmlElement(name = "shipping_address")
	private Long shippingAddress;

	@XmlElement(name = "credit_card")
	private Long creditCard;

	public NewOrderR() {
	}

	public List<ItemCartR> getItems() {
		return items;
	}

	public void setItems(List<ItemCartR> items) {
		this.items = items;
	}

	public Long getCustomer() {
		return customer;
	}

	public void setCustomer(Long customer) {
		this.customer = customer;
	}

	public Long getShippingAddress() {
		return shippingAddress;
	}

	public void setShippingAddress(Long shippingAddress) {
		this.shippingAddress = shippingAddress;
	}

	public Long getCreditCard() {
		return creditCard;
	}

	public void setCreditCard(Long creditCard) {
		this.creditCard = creditCard;
	}
}
