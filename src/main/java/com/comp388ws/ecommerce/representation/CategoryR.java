package com.comp388ws.ecommerce.representation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.comp388ws.ecommerce.model.Category;

@XmlRootElement(name = "Category")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
public class CategoryR {

	private Long id;

	private String name;

	private String description;

	public CategoryR() {
	}

	public CategoryR(Category category) {
		this.setId(category.getId());
		this.setName(category.getName());
		this.setDescription(category.getDescription());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
