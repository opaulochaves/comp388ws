package com.comp388ws.ecommerce.representation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.comp388ws.ecommerce.model.Login;
import com.comp388ws.ecommerce.model.Seller;

@XmlRootElement(name = "Seller")
@XmlAccessorType(XmlAccessType.FIELD)
public class SellerR extends BaseR {

	private Long id;

	@XmlElement(name = "first_name")
	private String firstName;

	@XmlElement(name = "last_name")
	private String lastName;

	private String email;

	private String phone;

	private String username;

	private String password;

	@XmlElement(name = "company_name")
	private String companyName;

	private String type;

	public SellerR() {
	}

	/**
	 * Create a Seller representation with login info
	 * 
	 * @param seller
	 * @param login
	 */
	public SellerR(Seller seller, Login login) {
		this.setId(seller.getId());
		this.setEmail(seller.getEmail());
		this.setFirstName(seller.getFirstName());
		this.setLastName(seller.getLastName());
		this.setCompanyName(seller.getCompanyName());
		this.setPhone(seller.getPhone());
		this.setType(seller.getType());
		this.setUsername(login.getUsername());
		this.setPassword(login.getPassword()); // remove password from response
	}

	/**
	 * Create a Seller representation without login info
	 * 
	 * @param seller
	 */
	public SellerR(Seller seller) {
		this.setId(seller.getId());
		this.setEmail(seller.getEmail());
		this.setFirstName(seller.getFirstName());
		this.setLastName(seller.getLastName());
		this.setPhone(seller.getPhone());
		this.setCompanyName(seller.getCompanyName());
		this.setType(seller.getType());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
