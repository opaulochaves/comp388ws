package com.comp388ws.ecommerce.representation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.comp388ws.ecommerce.model.CreditCard;

@XmlRootElement(name = "CreditCard")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
public class CreditCardR {

	private Long id;

	@XmlElement(name = "card_type")
	private String cardType;

	@XmlElement(name = "card_number")
	private String cardNumber;

	@XmlElement(name = "security_code")
	private String securityCode;

	@XmlElement(name = "name_on_card")
	private String nameOnCard;

	@XmlElement(name = "exp_month")
	private int expMonth;

	@XmlElement(name = "exp_year")
	private int expYear;

	public CreditCardR() {
	}

	public CreditCardR(CreditCard creditCard) {
		this.setId(creditCard.getId());
		this.setCardType(creditCard.getCardType());
		this.setCardNumber(creditCard.getCardNumber());
		this.setNameOnCard(creditCard.getNameOnCard());
		this.setSecurityCode(creditCard.getSecurityCode());
		this.setExpMonth(creditCard.getExpMonth());
		this.setExpYear(creditCard.getExpYear());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getSecurityCode() {
		return securityCode;
	}

	public void setSecurityCode(String securityCode) {
		this.securityCode = securityCode;
	}

	public String getNameOnCard() {
		return nameOnCard;
	}

	public void setNameOnCard(String nameOnCard) {
		this.nameOnCard = nameOnCard;
	}

	public int getExpMonth() {
		return expMonth;
	}

	public void setExpMonth(int expMonth) {
		this.expMonth = expMonth;
	}

	public int getExpYear() {
		return expYear;
	}

	public void setExpYear(int expYear) {
		this.expYear = expYear;
	}

}
