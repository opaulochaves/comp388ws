package com.comp388ws.ecommerce.representation;

import java.time.LocalDateTime;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.comp388ws.ecommerce.model.Review;
import com.comp388ws.ecommerce.util.LocalDateTimeXmlAdapter;

@XmlRootElement(name = "ProductReview")
@XmlAccessorType(XmlAccessType.FIELD)
public class ProductReviewR {

	private Long id;

	private String title;

	private String body;

	private int rating;

	@XmlElement(name = "created_at")
	@XmlJavaTypeAdapter(value = LocalDateTimeXmlAdapter.class)
	private LocalDateTime createdAt;

	private Long customer;
	
	@XmlElement(name = "user")
	private String customerName;

	private Long product;

	public ProductReviewR() {
	}

	public ProductReviewR(Review review) {
		this.setId(review.getId());
		this.setTitle(review.getTitle());
		this.setBody(review.getBody());
		this.setRating(review.getRating());
		this.setCreatedAt(review.getCreatedAt());
		this.setCustomer(review.getCustomer().getId());
		this.setCustomerName(review.getCustomer().getFirstName() + " " + review.getCustomer().getLastName());
		this.setProduct(review.getProduct().getId());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}

	public Long getCustomer() {
		return customer;
	}

	public void setCustomer(Long customer) {
		this.customer = customer;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public Long getProduct() {
		return product;
	}

	public void setProduct(Long product) {
		this.product = product;
	}
}
