package com.comp388ws.ecommerce.representation;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.PrePersist;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.comp388ws.ecommerce.model.Product;
import com.comp388ws.ecommerce.model.ProductDetail;
import com.comp388ws.ecommerce.util.LocalDateTimeXmlAdapter;

/**
 * 
 * @author Onontsatsal
 *
 */
@XmlRootElement(name = "Product")
@XmlAccessorType(XmlAccessType.FIELD)
public class ProductR extends BaseR {

	private Long id;

	private String name;

	private String description;

	@XmlElement(name = "unit_price")
	private BigDecimal unitPrice;

	private Long quantity;

	@XmlElement(name = "brand_name")
	private String brandName;

	private Long category;

	private Long seller;
	
	@XmlElement(name = "seller_name")
	private String sellerName;

	@XmlElement(name = "available_from")
	@XmlJavaTypeAdapter(value = LocalDateTimeXmlAdapter.class)
	private LocalDateTime availableFrom;

	@XmlElement(name = "created_at")
	@XmlJavaTypeAdapter(value = LocalDateTimeXmlAdapter.class)
	private LocalDateTime createdAt;

	private Boolean active;

	private List<ProductDetailR> details;

	public ProductR() {
	}

	public ProductR(Product product) {
		this.setId(product.getId());
		this.setName(product.getName());
		this.setDescription(product.getDescription());
		this.setUnitPrice(product.getUnitPrice());
		this.setQuantity(product.getQuantity());
		this.setBrandName(product.getBrandName());
		this.setCategory(product.getCategory().getId());
		this.setSeller(product.getSeller().getId());
		this.setSellerName(product.getSeller().getFirstName());
		this.setAvailableFrom(product.getAvailableFrom());
		this.setCreatedAt(product.getCreatedAt());
		this.setActive(product.isActive());
		this.setDetails(getDetails(product));
	}

	private List<ProductDetailR> getDetails(Product product) {
		List<ProductDetailR> detailsR = new ArrayList<>();
		List<ProductDetail> pDetails = product.getDetails();
		if (pDetails != null) {
			for (ProductDetail detail : product.getDetails()) {
				detailsR.add(new ProductDetailR(detail));
			}
		}
		return detailsR;
	}

	@PrePersist
	public void prePersit() {
		createdAt = LocalDateTime.now();
		this.setActive(true);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public BigDecimal getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}

	public Long getQuantity() {
		return quantity;
	}

	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public Long getCategory() {
		return category;
	}

	public void setCategory(Long categoryId) {
		this.category = categoryId;
	}

	public Long getSeller() {
		return seller;
	}

	public void setSeller(Long sellerId) {
		this.seller = sellerId;
	}

	public LocalDateTime getAvailableFrom() {
		return availableFrom;
	}

	public void setAvailableFrom(LocalDateTime availableFrom) {
		this.availableFrom = availableFrom;
	}

	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public List<ProductDetailR> getDetails() {
		return details;
	}

	public void setDetails(List<ProductDetailR> details) {
		this.details = details;
	}

	public String getSellerName() {
		return sellerName;
	}

	public void setSellerName(String sellerName) {
		this.sellerName = sellerName;
	}

}
