package com.comp388ws.ecommerce.representation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.comp388ws.ecommerce.model.Login;

@XmlRootElement(name = "User")
@XmlAccessorType(XmlAccessType.FIELD)
public class UserR {

	private String username;

	private String email;

	private String password;

	@XmlElement(name = "user_id")
	private Long userId;

	private String type;

	public UserR() {
	}

	public UserR(Login user) {
		this.setUsername(user.getUsername());
		this.setEmail(user.getEmail());

		if (!user.getUsername().equals("admin")) {
			if (user.getCustomer() != null) {
				this.setType("customer");
				this.setUserId(user.getCustomer().getId());
			} else {
				this.setType("seller");
				this.setUserId(user.getSeller().getId());
			}
		}
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}
