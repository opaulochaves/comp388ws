package com.comp388ws.ecommerce.representation;

import java.time.LocalDateTime;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.comp388ws.ecommerce.model.Review;
import com.comp388ws.ecommerce.util.LocalDateTimeXmlAdapter;

@XmlRootElement(name = "SellerReview")
@XmlAccessorType(XmlAccessType.FIELD)
public class SellerReviewR {

	private Long id;

	private String title;

	private String body;

	private int rating;

	@XmlElement(name = "created_at")
	@XmlJavaTypeAdapter(value = LocalDateTimeXmlAdapter.class)
	private LocalDateTime createdAt;

	@XmlElement(name = "customer")
	private Long customerId;

	@XmlElement(name = "user")
	private String customer;

	private Long seller;

	public SellerReviewR() {
	}

	public SellerReviewR(Review review) {
		this.setId(review.getId());
		this.setTitle(review.getTitle());
		this.setBody(review.getBody());
		this.setRating(review.getRating());
		this.setCreatedAt(review.getCreatedAt());
		this.setCustomerId(review.getCustomer().getId());
		this.setCustomer(review.getCustomer().getFirstName() + " " + review.getCustomer().getLastName());
		this.setSeller(review.getSeller().getId());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getCustomer() {
		return customer;
	}

	public void setCustomer(String customer) {
		this.customer = customer;
	}

	public Long getSeller() {
		return seller;
	}

	public void setSeller(Long sellerId) {
		this.seller = sellerId;
	}
}
