package com.comp388ws.ecommerce.repositories;

import org.springframework.data.repository.CrudRepository;

import com.comp388ws.ecommerce.model.Seller;

public interface SellerRepository extends CrudRepository<Seller, Long> {

	public Seller findByFirstNameContaining(String firstName);

}
