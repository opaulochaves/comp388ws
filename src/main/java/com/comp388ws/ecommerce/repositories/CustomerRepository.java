package com.comp388ws.ecommerce.repositories;

import org.springframework.data.repository.CrudRepository;

import com.comp388ws.ecommerce.model.Customer;

public interface CustomerRepository extends CrudRepository<Customer, Long> {

	public Customer findByFirstNameOrLastNameContaining(String firstName, String lastName);

}
