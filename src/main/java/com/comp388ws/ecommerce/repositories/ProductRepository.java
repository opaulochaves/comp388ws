package com.comp388ws.ecommerce.repositories;

import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.comp388ws.ecommerce.model.Product;
import com.comp388ws.ecommerce.model.Seller;

public interface ProductRepository extends CrudRepository<Product, Long> {

	/**
	 * Returns a list of products name or description matches and active is true
	 * 
	 * @param search
	 * @return
	 */
	@Query("SELECT p FROM Product p WHERE (p.name LIKE %?1% OR p.description LIKE %?1%) AND p.active is true")
	public List<Product> findByNameOrDescriptionAndActiveIsTrue(String search);

	public List<Product> findBySeller(Seller seller);

	public List<Product> findByIdIn(Collection<Long> ids);

}
