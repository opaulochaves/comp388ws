package com.comp388ws.ecommerce.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.comp388ws.ecommerce.model.Customer;
import com.comp388ws.ecommerce.model.Order;
import com.comp388ws.ecommerce.model.Seller;

public interface OrderRepository extends CrudRepository<Order, Long> {

	public Iterable<Order> findByCustomer(Customer customer);

	/**
	 * 
	 * @param seller
	 * @return
	 */
	@Query("SELECT DISTINCT o FROM Order o JOIN FETCH o.items i "
			+ "JOIN FETCH i.product p WHERE p.seller = ?1 AND i.status = '2'")
	public List<Order> findOrderForFulfillment(Seller seller);

	/**
	 * Find an order to be fulfilled that matches seller, item and status equals
	 * to Fulfillment (2). Even if an order has many items this query only
	 * returns one item for the order allowing this order.getItems().get(0)
	 * 
	 * @param seller
	 * @param itemId
	 * @return
	 */
	@Query("SELECT DISTINCT o FROM Order o JOIN FETCH o.items i "
			+ "JOIN FETCH i.product p WHERE p.seller = ?1 AND i.id = ?2 AND i.status = '2'")
	public Order findOrderForFulfillment(Seller seller, Long itemId);

	//@Query("SELECT o FROM Order o JOIN o.status os WHERE os.status = 1 AND o.customer = ?1")
	@Query("SELECT o FROM Order o WHERE o.customer = ?1 AND o.id NOT IN (SELECT os.order FROM OrderStatus os GROUP BY os.order HAVING count(1) > 1)")
	public List<Order> findOrderInProgress(Customer customer);

}
