package com.comp388ws.ecommerce.repositories;

import org.springframework.data.repository.CrudRepository;

import com.comp388ws.ecommerce.model.Customer;
import com.comp388ws.ecommerce.model.Login;
import com.comp388ws.ecommerce.model.Seller;

public interface LoginRepository extends CrudRepository<Login, Long> {

	public Login findByEmail(String email);

	public Login findByUsername(String username);

	public Login findByUsernameOrEmail(String username, String email);

	public Login findByUsernameAndPassword(String username, String password);

	public Login findByCustomer(Customer customer);
	
	public Login findBySeller(Seller seller);
}
