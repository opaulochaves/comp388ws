package com.comp388ws.ecommerce.repositories;

import org.springframework.data.repository.CrudRepository;

import com.comp388ws.ecommerce.model.Cart;

public interface CartRepository extends CrudRepository<Cart, Long> {

}
