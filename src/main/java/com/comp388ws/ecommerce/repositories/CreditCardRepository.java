package com.comp388ws.ecommerce.repositories;

import org.springframework.data.repository.CrudRepository;

import com.comp388ws.ecommerce.model.CreditCard;
import com.comp388ws.ecommerce.model.Customer;

public interface CreditCardRepository extends CrudRepository<CreditCard, Long> {

	/**
	 * Get the customer's credit card that is active
	 * 
	 * @param customer
	 * @param active
	 * @return
	 */
	public CreditCard findByCustomerAndActiveIsTrue(Customer customer);

}
