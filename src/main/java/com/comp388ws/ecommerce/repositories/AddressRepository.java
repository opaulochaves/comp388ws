package com.comp388ws.ecommerce.repositories;

import org.springframework.data.repository.CrudRepository;

import com.comp388ws.ecommerce.model.Address;

public interface AddressRepository extends CrudRepository<Address, Long> {

}
