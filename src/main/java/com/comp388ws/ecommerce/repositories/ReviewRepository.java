package com.comp388ws.ecommerce.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.comp388ws.ecommerce.model.Customer;
import com.comp388ws.ecommerce.model.Product;
import com.comp388ws.ecommerce.model.Review;
import com.comp388ws.ecommerce.model.Seller;

public interface ReviewRepository extends CrudRepository<Review, Long> {

	/**
	 * Returns all reviews of a product
	 * 
	 * @param product
	 * @return
	 */
	List<Review> findByProduct(Product product);

	/**
	 * Returns a review if a customer has reviewed a product
	 * 
	 * @param product
	 * @param customer
	 * @return
	 */
	Review findByProductAndCustomer(Product product, Customer customer);

	/**
	 * Return all reviews of a seller
	 * 
	 * @param seller
	 * @return
	 */
	List<Review> findBySeller(Seller seller);

	/**
	 * Returns a review if a customer has reviewed a seller
	 * 
	 * @param product
	 * @param customer
	 * @return
	 */
	Review findBySellerAndCustomer(Seller seller, Customer customer);
}
