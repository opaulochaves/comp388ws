package com.comp388ws.ecommerce.repositories;

import org.springframework.data.repository.CrudRepository;

import com.comp388ws.ecommerce.model.Category;

public interface CategoryRepository extends CrudRepository<Category, Long> {

	public Category findByName(String name);

	public Category findByNameContaining(String name);

	public Category findByDescription(String description);
}
