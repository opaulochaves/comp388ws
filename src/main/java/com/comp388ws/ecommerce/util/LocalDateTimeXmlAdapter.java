package com.comp388ws.ecommerce.util;

import java.time.LocalDateTime;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class LocalDateTimeXmlAdapter extends XmlAdapter<String, LocalDateTime> {

	@Override
	public String marshal(LocalDateTime arg0) throws Exception {
		return arg0.toString();
	}

	/**
	 * Example: 2015-10-30T00:00:00
	 */
	@Override
	public LocalDateTime unmarshal(String arg0) throws Exception {
		return LocalDateTime.parse(arg0);
	}
}