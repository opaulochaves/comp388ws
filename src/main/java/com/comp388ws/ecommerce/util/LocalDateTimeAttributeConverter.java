package com.comp388ws.ecommerce.util;

import java.sql.Timestamp;
import java.time.LocalDateTime;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * http://www.thoughts-on-java.org/persist-localdate-localdatetime-jpa/
 * 
 * @author pchavesdasilvafilho
 *
 */
@Converter(autoApply = true)
public class LocalDateTimeAttributeConverter implements AttributeConverter<LocalDateTime, Timestamp> {

	@Override
	public Timestamp convertToDatabaseColumn(LocalDateTime localDateTime) {
		return (localDateTime == null ? null : Timestamp.valueOf(localDateTime));
	}

	@Override
	public LocalDateTime convertToEntityAttribute(Timestamp sqlTimeStamp) {
		return (sqlTimeStamp == null ? null : sqlTimeStamp.toLocalDateTime());
	}

}
