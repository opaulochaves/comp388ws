DELETE FROM reviews;
DELETE FROM item_cart;
DELETE FROM order_products;
DELETE FROM orders_status;
DELETE FROM orders;
DELETE FROM product_details;
DELETE FROM products;
DELETE FROM categories;
DELETE FROM addresses;
DELETE FROM credit_cards;
DELETE FROM login;
DELETE FROM customers;
DELETE FROM cart;
DELETE FROM sellers;

INSERT INTO login (id, active, admin, created_at, email, password, username) VALUES (1, true, true, now(), 'admin@rest.com', '11111', 'admin');

/* Insert two sellers */
INSERT INTO sellers (id, active, company_name, email, first_name, last_name, phone, sellers_type) VALUES (1, true, 'The Tech', 'ttech@rest.com', 'Zion', 'William', '1233451111', 'Personal');
INSERT INTO sellers (id, active, company_name, email, first_name, last_name, phone, sellers_type) VALUES (2, true, 'Only Clothes', 'oclothes@rest.com', 'Mary', 'First', '9993452222', 'Personal');

/* Insert a login for each seller */
INSERT INTO login (id, active, admin, created_at, email, password, username, sellers_id) VALUES (2, true, false, now(), 'ttech@rest.com', '12345', 'zion', 1);
INSERT INTO login (id, active, admin, created_at, email, password, username, sellers_id) VALUES (3, true, false, now(), 'oclothes@rest.com', '12345', 'mary', 2);

/* Insert four customers */
INSERT INTO customers (id, active, email, first_name, last_name, phone) VALUES (1, true, 'paulo@rest.com', 'Paulo', 'Chaves', '1235559999');
INSERT INTO customers (id, active, email, first_name, last_name, phone) VALUES (2, true, 'ono@rest.com', 'Ono', 'Gantsog', '1234447777');
INSERT INTO customers (id, active, email, first_name, last_name, phone) VALUES (3, true, 'aman@rest.com', 'Aman', 'Meghrajani', '1232224455');
INSERT INTO customers (id, active, email, first_name, last_name, phone) VALUES (4, true, 'peter@rest.com', 'Peter', 'Pan', '1232227733');

/* Insert a login for each customer */
INSERT INTO login (id, active, admin, created_at, email, password, username, customers_id) VALUES (4, true, false, now(), 'paulo@rest.com', '12345', 'paulo', 1);
INSERT INTO login (id, active, admin, created_at, email, password, username, customers_id) VALUES (5, true, false, now(), 'ono@rest.com', '54321', 'ono', 2);
INSERT INTO login (id, active, admin, created_at, email, password, username, customers_id) VALUES (6, true, false, now(), 'aman@rest.com', '74174', 'aman', 3);
INSERT INTO login (id, active, admin, created_at, email, password, username, customers_id) VALUES (7, true, false, now(), 'peter@rest.com', '98765', 'peter', 4);

/* Insert a credit card for each customer, except for the last one */
INSERT INTO credit_cards (id, active, card_number, card_type, exp_month, exp_year, name_on_card, security_code, customers_id) VALUES (1, true, '1111222211112222', 'Visa', 12, 2020, 'PAULO C', '123', 1);
INSERT INTO credit_cards (id, active, card_number, card_type, exp_month, exp_year, name_on_card, security_code, customers_id) VALUES (2, true, '2222111122221111', 'Visa', 11, 2019, 'ONO G', '989', 2);
INSERT INTO credit_cards (id, active, card_number, card_type, exp_month, exp_year, name_on_card, security_code, customers_id) VALUES (3, true, '8888777711111111', 'Master Card', 10, 2020, 'AMAN M', '789', 3);

/* Insert addresses for all customers */
INSERT INTO addresses (id, city, country, line1, line2, state, type, zipcode, customers_id) VALUES (1, 'Chicago', 'US', '6300 N Loyola University St', null, 'IL', 'Home', '60660', 1);
INSERT INTO addresses (id, city, country, line1, line2, state, type, zipcode, customers_id) VALUES (2, 'Chicago', 'US', '6190 N Edgewater Ave', null, 'IL', 'Home', '60610', 2);
INSERT INTO addresses (id, city, country, line1, line2, state, type, zipcode, customers_id) VALUES (3, 'Chicago', 'US', '6315 N Rogers Park St', null, 'IL', 'Home', '60620', 3);
INSERT INTO addresses (id, city, country, line1, line2, state, type, zipcode, customers_id) VALUES (4, 'Chicago', 'US', '9999 S Never Land Ave', null, 'IL', 'Home', '60650', 4);

INSERT into categories (id, name, description) VALUES (1, 'Men Pants', 'Very good quality pants for men');
INSERT into categories (id, name, description) VALUES (2, 'Women Dresses', 'The best dresses in the world');
INSERT into categories (id, name, description) VALUES (3, 'Women Shoes', 'The most durable and confortable shoes');
INSERT into categories (id, name, description) VALUES (4, 'Smartphones', 'All kinds of smartphones');
INSERT into categories (id, name, description) VALUES (5, 'Laptops', 'We have very good and cheap laptops');

INSERT INTO products (id, active, available_from, brand_name, created_at, description, name, quantity, unit_price, categories_id, sellers_id) VALUES (1, true, now(), 'Levis', now(), 'Regular Fit Jean  is a regular fit jean with a straight leg', 'Regular Fit Jean', 100, 39.49, 1, 2);
INSERT INTO products (id, active, available_from, brand_name, created_at, description, name, quantity, unit_price, categories_id, sellers_id) VALUES (2, true, now(), 'South Pole', now(), 'Active Basic Jogger Fleece Pants', 'Active Basic Jogger Fleece Pants', 100, 15.79, 1, 2);
INSERT INTO products (id, active, available_from, brand_name, created_at, description, name, quantity, unit_price, categories_id, sellers_id) VALUES (3, true, now(), 'Donna Morgan', now(), 'Donna Morgan Womens Short-Sleeve Dupioni Fit-And-Flare Dress with Floral Print', 'Short-Sleeve Dupioni Fit-And-Flare Dress with Floral Print', 50, 158, 2, 2);
INSERT INTO products (id, active, available_from, brand_name, created_at, description, name, quantity, unit_price, categories_id, sellers_id) VALUES (4, true, now(), 'BB Dakota', now(), 'BB Dakota Womens Ruth Printed Fit and Flare Midi Dress', 'Ruth Printed Fit and Flare Midi Dress', 75, 70.99, 2, 2);
INSERT INTO products (id, active, available_from, brand_name, created_at, description, name, quantity, unit_price, categories_id, sellers_id) VALUES (5, true, now(), 'adidas', now(), 'adidas Performance Womens Vigor 5 TR W Trail Running Shoe', 'Vigor 5 TR W Trail Running Shoe', 30, 59.99, 3, 2);
INSERT INTO products (id, active, available_from, brand_name, created_at, description, name, quantity, unit_price, categories_id, sellers_id) VALUES (6, true, now(), 'Apple', now(), 'iPhone 6s 16GB', 'iPhone6s 16GB', 30, 650, 4, 1);
INSERT INTO products (id, active, available_from, brand_name, created_at, description, name, quantity, unit_price, categories_id, sellers_id) VALUES (7, true, now(), 'Samsung', now(), 'Galaxy Note 5', 'Galaxy Note 5', 20, 745.99, 4, 1);
INSERT INTO products (id, active, available_from, brand_name, created_at, description, name, quantity, unit_price, categories_id, sellers_id) VALUES (8, true, now(), 'Apple', now(), '15-inch MacBook Pro with Retina display', '15-inch MacBook Pro', 50, 139.95, 5, 1);
INSERT INTO products (id, active, available_from, brand_name, created_at, description, name, quantity, unit_price, categories_id, sellers_id) VALUES (9, true, now(), 'Dell', now(), 'New XPS 15 Non-Touch', 'New XPS 15 Non-Touch', 15, 1199, 5, 1);

INSERT INTO reviews (id, rating, created_at, customers_id, sellers_id, products_id, title, body) VALUES (1, 5, now(), 1, null, 8, 'Lorem ipsum dolor sit amet', 'Lorem ipsum dolor sit amet, id has nusquam disputationi, an tantas antiopam qui. Usu an prima denique, indoctum splendide ea mei. Mel ex quodsi doctus iriure, feugait eleifend cum id, et est integre molestie. Animal nusquam neglegentur nec ad, ius ex enim affert commune. Malorum nostrud qui cu, feugait repudiare prodesset per an.');
INSERT INTO reviews (id, rating, created_at, customers_id, sellers_id, products_id, title, body) VALUES (2, 5, now(), 2, null, 6, 'Ius eu assum deleniti, expetenda persecuti', 'Ius eu assum deleniti, expetenda persecuti vis eu. Senserit consequat ne cum, est facer postea an. Eu viris facilis qui. Doming luptatum quaerendum cu est, id mea quem quas nullam. No quo graeci nostro. Assum antiopam maiestatis te ius.');
INSERT INTO reviews (id, rating, created_at, customers_id, sellers_id, products_id, title, body) VALUES (3, 5, now(), 1, 1, null, 'endide sed, possit iudicabit id duo', 'Ex dicit maiestatis sit, at tamquam dignissim splendide sed, possit iudicabit id duo. Vix sumo saperet eu, magna meliore ne mei, cu cum tollit intellegam delicatissimi.');
INSERT INTO reviews (id, rating, created_at, customers_id, sellers_id, products_id, title, body) VALUES (4, 5, now(), 2, 2, null, 'Te mei nemore vituperatoribus', 'Te detracto offendit ius, ut laudem definitionem est. Te mei nemore vituperatoribus, ei vis tota legendos posidonium. At his placerat singulis assentior, ad quodsi minimum splendide ius.');
